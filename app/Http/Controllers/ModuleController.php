<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ModuleController extends Controller
{

    //Module Insert Page View
    public function index() {
        return view('backend.module');
    }


    //Save Module into DB
    public function storeModule(Request $request)
    {
        $this->validate($request, [
            'module_name'  =>  'required'
        ]);

        DB::table('modules')->insert([
    		'module_name'	=>	$request->module_name
        ]);
        
        return redirect()->back()->with('success','Module name save successfully.');
    }
    
    //Get All Modules
    public function getAllModules() {
        $modules = DB::table('modules')->get();
        return view('backend.moduleall',compact('modules'));
    }


    //Get Module list
    public function getModule()
    {
        return DB::table('modules')->get();
    }

    //Delete Module
    public function delModule($id) {

        $child = DB::table('module_wise_role')->where('module_id',$id)->get();

        if($child->count() == 0){
            DB::table('modules')->where('id', $id)->delete();
        } elseif($child->count() > 0) {
            DB::table('module_wise_role')->where('module_id', $id)->delete();
            DB::table('modules')->where('id', $id)->delete();
        }

        return redirect()->back()->with('success','One module delete successfully.');
    }


    //Get Module by Id
    public function getModuleById($id)
    {
        return  DB::table('modules')->where('id',$id)->get();
    }


    //Update Module
    public function updateModule(Request $request,$id)
    {
        $this->validate($request, [
            'module_name'  =>  'required'
        ]);

        DB::table('modules')
            ->where('id', $id)
            ->update([
                'module_name' => $request->module_name
                ]);

        return redirect()->back()->with('success','Module name updated successfully.');
    }


    //Module Wise Role View
    public function moduleWiseRoleView() {
        $modules = DB::table('modules')->get();
        return view('backend.modulewiserole',compact('modules'));
    }


    //Save Module wise role
    public function storeModuleWiseRole(Request $request)
    {
        $this->validate($request, [
            'module_id'     => 'required',
            'role_name'     => 'required'
        ]);

        DB::table('module_wise_role')->insert([
            'module_id'     =>  $request->module_id,
            'role_name'     =>  $request->role_name
        ]);

        return redirect()->back()->with('success','Module wise role add successfully.');
    }



    //Get Module Wise Role List
    public function getModuleWiseRole() {

        $roles =  DB::table('modules')
                    ->join('module_wise_role', 'modules.id', '=', 'module_wise_role.module_id')
                    ->select('modules.*', 'module_wise_role.*')
                    ->get();
        $modules = DB::table('modules')->get();

        return  view('backend.modulewiseroleall',compact('roles','modules'));            
    }


    //Delete Module Wise Role
    public function delModuleWiseRole($id) {
        DB::table('module_wise_role')->where('id', $id)->delete();
        return redirect()->back()->with('success','Role delete successfully.');
    }


    //Update Module Wise Role
    public function updateModuleWiseRole(Request $request,$id)
    {
        $this->validate($request, [
            'module_id'     => 'required',
            'role_name'     => 'required'
        ]);
        
        DB::table('module_wise_role')
            ->where('id', $id)
            ->update([
                'module_id'     => $request->module_id,
                'role_name'     => $request->role_name,
                ]);
                
        return redirect()->back()->with('success','Module wise role name updated successfully.');
    }



}
