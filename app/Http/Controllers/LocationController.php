<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Requests\LocationRequest;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    //Get All Clinic/Location List
    public function getAllLocations() {

        $locations =  DB::table('locations')
                        ->join('countries','locations.country','=','countries.id')
                        ->join('states','locations.state','=','states.id')
                        ->join('cities','locations.city','=','cities.id')
                        ->select('locations.*','countries.country_name as country','states.state_name as state','cities.city_name as city')
                        ->get();


        return view('backend.locationall',compact('locations'));

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = DB::table('countries')->get();

        return view('backend.location',compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        $location = new Location();

        $location->admin_id             =   $request->admin_id;
        $location->clinic_name          =   $request->clinic_name;
        $location->email                =   $request->email;
        $location->password             =   bcrypt($request->password);
        $location->phone                =   $request->phone;
        $location->website              =   $request->website;
        $location->country              =   $request->country;
        $location->state                =   $request->state;
        $location->city                 =   $request->city;
        $location->address              =   $request->address;
        $location->post_code            =   $request->post_code;
        $location->controlled_license   =   $request->controlled_license;
        $location->reg_no               =   $request->reg_no;
        $location->profile              =   $request->profile;

        $location->save();

        return redirect()->back()->with('success','Location/Clinic add successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {

    }

    //Location Delete
    public function deleteLocation($id) {
        Location::where('id',$id)->delete();
        return redirect()->back()->with('success','Location/Clinic/Hospital Delete Successfully.');
    }


    //Location Update
    public function updateLocation(Request $request, $id) {
        

        $this->validate($request, [
            'clinic_name'       => 'required',
            'phone'             => 'required',
            'address'            => 'required'
        ]);

        $location = Location::find($id);

        $location->clinic_name      =   $request->clinic_name;
        $location->phone            =   $request->phone;
        $location->address          =   $request->address;

        $location->save();

        return redirect()->back()->with('success','Clinic/Location Information Update Successfully.');

    }

}
