<?php

namespace App\Http\Controllers;

use App\Qsection;
use Illuminate\Http\Request;

class QsectionController extends Controller
{
    //Get All Sections
    public function getAllSections() {
        $sections = Qsection::all();
        return view('backend.sectionall',compact('sections'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.section');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'    =>'required'
        ]);

        $section = new Qsection;

        $section->title             =   $request->title;
        $section->sub_title         =   $request->sub_title;
        $section->description       =   $request->description;
        $section->placement_step    =   $request->placement_step;
        $section->placement_order   =   $request->placement_order;
        $section->is_required       =   $request->is_required;

        $section->save();

        return redirect()->back()->with('success','Question Section added successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Qsection  $qsection
     * @return \Illuminate\Http\Response
     */
    public function show(Qsection $qsection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Qsection  $qsection
     * @return \Illuminate\Http\Response
     */
    public function edit(Qsection $qsection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Qsection  $qsection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Qsection $qsection)
    {

    }

    //Section Update
    public function updateSection(Request $request, $id) {

        $this->validate($request, [
            'title'         =>      'required'
        ]);

        $section = Qsection::find($id);
        
        $section->title         =   $request->title;
        $section->sub_title     =   $request->sub_title;
        $section->description   =   $request->description;

        $section->save();

        return redirect()->back()->with('success','Section Information Update Successfully.');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Qsection  $qsection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Qsection $qsection)
    {

    }

    //Section Delete
    public function deleteSection($id) {
        Qsection::where('id',$id)->delete();
        return redirect()->back()->with('success','Section Delete Successfully.');
    }



}