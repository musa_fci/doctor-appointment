<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhysicianRequest;
use App\Physician;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PhysicianController extends Controller
{
    //Get All Physician
    public function getAllPhysicians() {

        $physicians =  DB::table('physicians')
                        ->join('locations','physicians.location_id','=','locations.id')
                        ->select('physicians.*','locations.clinic_name as clinic_name')
                        ->get();

        $locations =  DB::table('locations')->get();

        return view('backend.physicianall',compact('physicians','locations'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = DB::table('locations')->get();
        return view('backend.physician',compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhysicianRequest $request)
    {
        $physician =  new Physician;

        $physician->admin_id        =      $request->admin_id;
        $physician->location_id     =      $request->location_id;
        $physician->name            =      $request->name;
        $physician->email           =      $request->email;
        $physician->password        =      bcrypt($request->password);
        $physician->phone           =      $request->phone;
        $physician->license         =      $request->license;
        $physician->notification    =      $request->notification;

        $physician->save();

        return redirect()->back()->with('success','Physician added successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function show(Physician $physician)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function edit(Physician $physician)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Physician $physician)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function destroy(Physician $physician)
    {
        Physician::where('id',$physician->id)->delete();

        return response()->json([
            'msg'   =>  'Physician Delete Successfully.'
        ]);
    }

    //Physician Delete
    public function deletePhysician($id) {
        Physician::where('id',$id)->delete();
        return redirect()->back()->with('success','Physician Delete Successfully.');
    }

    //Physician Update
    public function updatePhysician(Request $request,$id) {

        $this->validate($request, [
                'location_id'       =>      'required',
                'name'              =>      'required',
                'email'             =>      'required|email',
                'phone'             =>      'required',
                'license'           =>      'required',
        ]);

        $physician = Physician::find($id);

        $physician->location_id    =   $request->location_id;
        $physician->name           =   $request->name;
        $physician->email          =   $request->email;
        $physician->phone          =   $request->phone;
        $physician->license        =   $request->license;

        $physician->save();

        return redirect()->back()->with('success','Physician Information Update Successfully.');

    }
}
