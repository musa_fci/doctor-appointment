<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientRequest;
use App\Location;
use App\Patient;
use App\Physician;
use App\Qsection;
use App\Question;
Use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use Authy\AuthyApi as AuthyApi;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\MessageBag;
use Twilio\Rest\Client;

use function React\Promise\all;

use PDF;

class PatientController extends Controller
{

    //Patient sign in page
    public function patientSignIn()
    {
        return view('frontend/signin');
    }

    
    //Patient Login and Re-direct to Patient Profile
    public function patientLogin(Request $request)
    {
        $this->validate($request, [
            'email'    =>'required',
            'password' => 'required'
        ]);
        if(Auth::guard('patient')->attempt(['email'=>$request->email,'password'=>$request->password],true))
        {
            return redirect('/patientprofile')->with('success','You are login successfully.');
        } 
        return redirect()->back()->with('loginFail','Login information is not matched.');
    }


    //Get total question system for admin panel
    public function getQuestion()
    {
        return Qsection::with('questions.options')->get();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations  = Location::all();
        $sections = Qsection::with('questions.options')->get();
        $countries = DB::table('countries')->get();
        return view('frontend.signup',compact('locations','sections','countries'));
    }


    //Patient Add by Admin Get Request
    public function patientaddByAdminView()
    {
        $locations  = Location::all();
        $sections = Qsection::with('questions.options')->get();
        $countries = DB::table('countries')->get();
        return view('backend.patientadd',compact('locations','sections','countries'));
    }

    //Patient Add by Admin Post Request
    public function patientaddByAdmin(PatientRequest $request)
    {
        
        if (isset($request->patient_photo_id)){
            $base64_image = $request->input('patient_photo_id'); // your base64 encoded
            @list($type, $file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $patientPhotoId = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$patientPhotoId, base64_decode($file_data));
        } else {
            $patientPhotoId = '';
        }


        if (isset($request->state_id_front)) {
            $base64_image = $request->input('state_id_front');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',',$file_data);
            $stateIdFront = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$stateIdFront,base64_decode($file_data));
        } else {
            $stateIdFront = '';
        }


        if (isset($request->state_id_back)) {
            $base64_image = $request->input('state_id_back');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $stateIdBack = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$stateIdBack,base64_decode($file_data));
        } else {
            $stateIdBack = '';
        }


        if (isset($request->signature_data)) {
            $base64_image = $request->input('signature_data');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $signature_data = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/signature/'.$signature_data,base64_decode($file_data));
        } else {
            $signature_data = '';
        }


            $patient = new Patient;

            $patient->location_id           =   $request->location_id;
            $patient->physician_id          =   $request->physician_id;
            $patient->f_name                =   $request->f_name;
            $patient->l_name                =   $request->l_name;
            $patient->email                 =   $request->email;
            $patient->country_code          =   $request->country_code;
            $patient->phone_number          =   $request->phone_number;
            $patient->password              =   bcrypt($request->password);
            $patient->address               =   $request->address;
            $patient->country               =   $request->country;
            $patient->state                 =   $request->state;
            $patient->city                  =   $request->city;
            $patient->state_id              =   $request->state_id;
            $patient->zip_code              =   $request->zip_code;
            $patient->dob                   =   $request->dob;
            $patient->gender                =   $request->gender;
            $patient->initial_visit         =   $request->initial_visit;
            $patient->expiration            =   $request->expiration;
            $patient->caregiver             =   $request->caregiver;
            $patient->internal              =   $request->internal;
            $patient->social_security_no    =   $request->social_security_no;
            $patient->rec_type              =   $request->rec_type;
            $patient->guardian              =   $request->guardian;
            $patient->contact_option        =   $request->contact_option;
            $patient->referred_by           =   $request->referred_by;
            $patient->mmu_id                =   $request->mmu_id;
            $patient->state_id_front        =   $stateIdFront;
            $patient->state_id_back         =   $stateIdBack;
            $patient->patient_photo_id      =   $patientPhotoId;
            $patient->patient_note          =   $request->patient_note;
            $patient->signature_name        =   $request->signature_name;
            $patient->signature_data        =   $signature_data;
            $patient->verified              =   1;

            $patient->save();


            //Patient File
            if(isset($request->patient_file)) {
                foreach($request->patient_file as $key=>$file) {

                    $orgName = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $fileName = md5(uniqid(rand(), true)).'.'.$extension;
                    $file->storeAs('public/files',$fileName);

                    DB::table('patient_files')->insert([
                        'patient_id'	=>	$patient->id,
                        'file_name'		=>	$fileName,
                        'file_org_name' =>  $orgName,
                    ]);
                }
            }



            //medical_symtomps
            if(isset($request->medical_symtomps)) {

                foreach($request->symtomps_question_id as $key=>$symtomps_qid) {
                    
                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $symtomps_qid;
                    $answer->answer         =  json_encode($request->medical_symtomps);
                    $answer->save();
                }
            }


            //significant_issue-for input box
            if(isset($request->significant_issue)) {
                $key   = $request->significant_question_id;
                $value = $request->significant_issue;

                $data = array_combine($key,$value);

                foreach($data as $key=>$value) {
                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $key;
                    $answer->answer         =   $value;
                    $answer->save();
                }
            }


            //medical_info
            if(isset($request->medical_info)) {
                $key    =   $request->medical_info_question_id;
                $value  =   $request->medical_info;

                $data   =   array_combine($key,$value);

                foreach($data as $key=>$value) {
                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $key;
                    $answer->answer         =   $value;
                    $answer->save();
                }
            }

            return redirect()->back()->with('success','Patient General Information Saved successfully !');

    }

    //Patient Profile - After login patient can view his profile.
    public function patientprofile()
    {
        $patients   = DB::table('patients')
                        ->where('patients.id',Auth::guard('patient')->id())
                        ->join('locations','patients.location_id','=','locations.id')
                        ->join('physicians','patients.physician_id','=','physicians.id')
                        ->join('countries','patients.country','=','countries.id')
                        ->join('states','patients.state','=','states.id')
                        ->join('cities','patients.city','=','cities.id')
                        ->select('patients.*','locations.clinic_name','physicians.name as physician_name','countries.country_name as country','states.state_name as state','cities.city_name as city')
                        ->get();

        $files  =   DB::table('patient_files')
                        ->where('patient_id',Auth::guard('patient')->id())
                        ->get();

        $sections = Qsection::with('questions.options')->get();

        $answers    =   DB::table('answers')
                            ->where('answers.patient_id',Auth::guard('patient')->id())
                            ->join('questions','answers.question_id','=','questions.id')
                            ->select('answers.*','questions.*')
                            ->get();


        return view('frontend.patientprofile',compact('patients','files','sections','answers'));
    }



    //All Patient List with details
    public function getAllPatients()
    {

        $patients =  DB::table('patients')
                        ->where('deleted_at', NULL)
                        ->join('locations', 'locations.id', '=', 'patients.location_id')
                        ->join('physicians', 'physicians.id', '=', 'patients.physician_id')
                        ->join('cities', 'cities.id', '=', 'patients.city')
                        ->select('patients.*','locations.clinic_name','physicians.name as physician_name','cities.city_name')
                        ->orderBy('patients.id', 'ASC')
                        ->get();
        return view('backend.patientall',compact('patients'));
    }


    //Download Logged In Patient Info
    public function downloadPatientInfo() {

        $patients   = DB::table('patients')
                        ->where('patients.id',Auth::guard('patient')->id())
                        ->join('locations','patients.location_id','=','locations.id')
                        ->join('physicians','patients.physician_id','=','physicians.id')
                        ->join('countries','patients.country','=','countries.id')
                        ->join('states','patients.state','=','states.id')
                        ->join('cities','patients.city','=','cities.id')
                        ->select('patients.*','locations.clinic_name','physicians.name as physician_name','countries.country_name as country','states.state_name as state','cities.city_name as city')
                        ->get();

        $sections = Qsection::with('questions.options')->get();
        
        $answers    =   DB::table('answers')
        ->where('answers.patient_id',Auth::guard('patient')->id())
        ->join('questions','answers.question_id','=','questions.id')
        ->select('answers.*','questions.*')
        ->get();

        $pdf = PDF::loadView('backend.patientinfoPDF', compact('patients','sections','answers'));
        return $pdf->download('Patient-Info-File.pdf');
    }

    //Download Patient Information from Admin
    public function downloadPatientInfoIDWise($id) {
        
        $patients   = DB::table('patients')
                        ->where('patients.id',$id)
                        ->join('locations','patients.location_id','=','locations.id')
                        ->join('physicians','patients.physician_id','=','physicians.id')
                        ->join('countries','patients.country','=','countries.id')
                        ->join('states','patients.state','=','states.id')
                        ->join('cities','patients.city','=','cities.id')
                        ->select('patients.*','locations.clinic_name','physicians.name as physician_name','countries.country_name as country','states.state_name as state','cities.city_name as city')
                        ->get();

        $sections = Qsection::with('questions.options')->get();
        
        $answers    =   DB::table('answers')
        ->where('answers.patient_id',$id)
        ->join('questions','answers.question_id','=','questions.id')
        ->select('answers.*','questions.*')
        ->get();

        $pdf = PDF::loadView('backend.patientinfoPDF', compact('patients','sections','answers'));
        return $pdf->download('Patient-Info-File.pdf');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request,AuthyApi $authyApi)
    {

        if (isset($request->patient_photo_id)){
            $base64_image = $request->input('patient_photo_id'); // your base64 encoded
            @list($type, $file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $patientPhotoId = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$patientPhotoId, base64_decode($file_data));
        } else {
            $patientPhotoId = '';
        }


        if (isset($request->state_id_front)) {
            $base64_image = $request->input('state_id_front');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',',$file_data);
            $stateIdFront = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$stateIdFront,base64_decode($file_data));
        } else {
            $stateIdFront = '';
        }


        if (isset($request->state_id_back)) {
            $base64_image = $request->input('state_id_back');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $stateIdBack = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/images/'.$stateIdBack,base64_decode($file_data));
        } else {
            $stateIdBack = '';
        }


        if (isset($request->signature_data)) {
            $base64_image = $request->input('signature_data');
            @list($type,$file_data) = explode(';', $base64_image);
            @list(, $file_data) = explode(',', $file_data);
            $signature_data = md5(uniqid(rand(), true)).'.'.'png';
            Storage::disk('public')->put('/signature/'.$signature_data,base64_decode($file_data));
        } else {
            $signature_data = '';
        }


            $patient = new Patient;

            $patient->location_id           =   $request->location_id;
            $patient->physician_id          =   $request->physician_id;
            $patient->f_name                =   $request->f_name;
            $patient->l_name                =   $request->l_name;
            $patient->email                 =   $request->email;
            $patient->country_code          =   $request->country_code;
            $patient->phone_number          =   $request->phone_number;
            $patient->password              =   bcrypt($request->password);
            $patient->address               =   $request->address;
            $patient->country               =   $request->country;
            $patient->state                 =   $request->state;
            $patient->city                  =   $request->city;
            $patient->state_id              =   $request->state_id;
            $patient->zip_code              =   $request->zip_code;
            $patient->dob                   =   $request->dob;
            $patient->gender                =   $request->gender;
            $patient->initial_visit         =   $request->initial_visit;
            $patient->expiration            =   $request->expiration;
            $patient->caregiver             =   $request->caregiver;
            $patient->internal              =   $request->internal;
            $patient->social_security_no    =   $request->social_security_no;
            $patient->rec_type              =   $request->rec_type;
            $patient->guardian              =   $request->guardian;
            $patient->contact_option        =   $request->contact_option;
            $patient->referred_by           =   $request->referred_by;
            $patient->mmu_id                =   $request->mmu_id;
            $patient->state_id_front        =   $stateIdFront;
            $patient->state_id_back         =   $stateIdBack;
            $patient->patient_photo_id      =   $patientPhotoId;
            $patient->patient_note          =   $request->patient_note;
            $patient->signature_name        =   $request->signature_name;
            $patient->signature_data        =   $signature_data;

            $patient->save();


                //Patient File
                if(isset($request->patient_file)) {
                    foreach($request->patient_file as $key=>$file) {
    
                        $orgName = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $fileName = md5(uniqid(rand(), true)).'.'.$extension;
                        $file->storeAs('public/files',$fileName);
    
                        DB::table('patient_files')->insert([
                            'patient_id'	=>	$patient->id,
                            'file_name'		=>	$fileName,
                            'file_org_name' =>  $orgName,
                        ]);
                    }
                }



            //medical_symtomps
            if(isset($request->medical_symtomps)) {

                foreach($request->symtomps_question_id as $key=>$symtomps_qid) {

                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $symtomps_qid;
                    $answer->answer         =  json_encode($request->medical_symtomps);
                    $answer->save();
                }
            }



            //significant_issue-for input box
            if(isset($request->significant_issue)) {
                $key   = $request->significant_question_id;
                $value = $request->significant_issue;

                $data = array_combine($key,$value);

                foreach($data as $key=>$value) {
                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $key;
                    $answer->answer         =   $value;
                    $answer->save();
                }
            }


            //medical_info
            if(isset($request->medical_info)) {
                $key    =   $request->medical_info_question_id;
                $value  =   $request->medical_info;

                $data   =   array_combine($key,$value);

                foreach($data as $key=>$value) {
                    $answer = New Answer;
                    $answer->patient_id     =   $patient->id;
                    $answer->question_id    =   $key;
                    $answer->answer         =   $value;
                    $answer->save();
                }
            }


            //Implement OTP

            Auth::guard('patient')->login($patient);

            $authyUser = $authyApi->registerUser(
                $patient->email,
                $patient->phone_number,
                $patient->country_code
            );
            if ($authyUser->ok()) {
                $patient->authy_id = $authyUser->id();
                $patient->save();
                $request->session()->flash(
                    'status',
                    "User created successfully"
                );
    
                $sms = $authyApi->requestSms($patient->authy_id);
                // DB::commit();
                return redirect('/verifyPatient');
                // return response()->json([
                //     'msg'   => "success"
                // ]);
            } else {
                $errors = $this->getAuthyErrors($authyUser->errors());
                // DB::rollback();
                // return view('newUser', ['errors' => new MessageBag($errors)]);
                return response()->json([
                    'msg'   => "Failed"
                ]);
            }


            return redirect()->back()->with('success','Patient General Information Saved successfully !');
    }


    //Show verfify patient page
    public function verifyPatient() {
        return view('frontend.verifyPatient');
    }


    //Patient token submit
    public function verify(Request $request, Authenticatable $user, AuthyApi $authyApi, Client $client) {

        $this->validate($request, [
            'token'    =>'required'
        ]);

        $token = $request->input('token');
        $verification = $authyApi->verifyToken($user->authy_id, $token);

        if ($verification->ok()) {
            $user->verified = true;
            $user->save();
            $this->sendSmsNotification($client, $user);

            return redirect('/patientprofile');
            // return response()->json([
            //     'msg'   => "success"
            // ]);
        } else {
            $errors = $this->getAuthyErrors($verification->errors());
            return view('frontend.verifyPatient', ['errors' => new MessageBag($errors)]);
            // return response()->json([
            //     'msg'   => "Failed"
            // ]);
        }
    }


    public function verifyResend(Request $request, Authenticatable $user, AuthyApi $authyApi) {
        $sms = $authyApi->requestSms($user->authy_id);

        if ($sms->ok()) {
            $request->session()->flash(
            'status',
            'Verification code re-sent'
            );
            return redirect()->route('user-show-verify');
        } else {
            $errors = $this->getAuthyErrors($sms->errors());
            return view('verifyUser', ['errors' => new MessageBag($errors)]);
        }
    }


    private function getAuthyErrors($authyErrors)
    {
        $errors = [];
        foreach ($authyErrors as $field => $message) {
            array_push($errors, $field . ': ' . $message);
        }
        return $errors;
    }


    private function sendSmsNotification($client, $user)
    {
        $twilioNumber = config('services.twilio')['number'] or die(
            "TWILIO_NUMBER is not set in the environment"
        );
        $messageBody = 'You did it! Signup complete :)';

        $client->messages->create(
        $user->fullNumber(),    // Phone number which receives the message
        [
            "from" => $twilioNumber, // From a Twilio number in your account
            "body" => $messageBody
        ]
        );
    }










    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        //
    }


    //Patient Delete
    public function deletePatient($id) {
        Patient::where('id',$id)->delete();
        return redirect()->back()->with('success','Patient Delete Successfully.');
    }


    //Patient Update View
    public function updatePatient($id) {
        return view('backend.patientUpdate');
    }


    
}
