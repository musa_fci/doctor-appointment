<?php

namespace App\Http\Controllers;

use App\Question;
use App\Option;
use App\Qsection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{

    //Get All Question
    public function getAllQuestions() {

        $questions  =   DB::table('questions')
                        ->join('qsections', 'questions.section_id', '=', 'qsections.id')                    
                        ->select('questions.*', 'qsections.title as section_title')                   
                        ->get();

        return view('backend.questionall',compact('questions'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section =   DB::table('questions')
                    ->join('qsections', 'questions.section_id', '=', 'qsections.id')                    
                    ->select('questions.*', 'qsections.title as section_title')                   
                    ->get();

        $sections = Qsection::all();

        return view('backend.question',compact('sections'));
    }

    
    //Get Question Type
    public function getQuestionType($id) {
        $questionType = DB::table('questions')->where('id',$id)->get();
        return response()->json([
            'data'  =>  $questionType
        ]);
    }


    //Get ID Wise Question Details
    public function getQuestion() 
    {
        $questions = Question::with('options')->where('id',55)->get();

        return $questions;
        
        dd($questions);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'section_id'        =>'required',
            'question_type'     =>'required',
            'question'          =>'required',
        ]);

        $question = new Question;

        $question->section_id      =   $request->section_id;
        $question->title           =   $request->title;
        $question->sub_title       =   $request->sub_title;
        $question->description     =   $request->description;
        $question->question_type   =   $request->question_type;
        $question->question        =   $request->question;

        $question->save();

        if(isset($request->option)) {
            foreach ($request->option as $key => $value) {

                $option =   new Option;
                $option->question_id    =   $question->id;
                $option->question_type  =   $request->question_type;
                $option->option         =   $value;

                $option->save();
            }
        }

        return redirect()->back()->with('success','Question added successfully.');

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $this->validate($request, [
            'section_id'        =>'required',
            'question'          =>'required',
        ]);

        $question = Question::find($request->id);

        $question->section_id    =   $request->section_id;
        $question->title         =   $request->title;
        $question->sub_title     =   $request->sub_title;
        $question->description   =   $request->description;
        $question->question      =   $request->question;

        $question->save();

        return response()->json([
            'updateMsg'     =>  'Question Information Update Successfully.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {

    }

    //Question Delete
    public function deleteQuestion($id) {
        Option::where('question_id',$id)->delete();
        Question::where('id',$id)->delete();

        return redirect()->back()->with('success','Question Delete Successfully.');
    }

}
