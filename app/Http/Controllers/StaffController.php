<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaffRequest;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    //Get All Staffs
    public function getAllStaffs() {

        $staffs =  DB::table('staff')
                        ->join('locations','staff.location_id','=','locations.id')
                        ->join('staff_category','staff.category_id','=','staff_category.id')
                        ->select('staff.*','locations.clinic_name as clinic_name','staff_category.name as staff_cat_name')
                        ->get();

        $locations =  DB::table('locations')->get();
        $staffcats = DB::table('staff_category')->get();

        return view('backend.staffall',compact('staffs','locations','staffcats'));
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = DB::table('locations')->get();
        $staffcats = DB::table('staff_category')->get();
        return view('backend.staff',compact('locations','staffcats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaffRequest $request)
    {
        $staff = new Staff;

        $staff->admin_id        =   $request->admin_id;
        $staff->location_id     =   $request->location_id;
        $staff->category_id     =   $request->category_id;
        $staff->name            =   $request->name;
        $staff->email           =   $request->email;
        $staff->password        =   bcrypt($request->password);
        $staff->phone           =   $request->phone;
        $staff->notification    =   $request->notification;

        $staff->save();

        return redirect()->back()->with('success','Staff added successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staff)
    {

    }

    //Staff Delete
    public function deleteStaff($id) {
        Staff::where('id',$id)->delete();
        return redirect()->back()->with('success','Staff Delete Successfully.');
    }

    //Staff Update
    public function updateStaff(Request $request, $id) {

        $this->validate($request, [
                'location_id'       =>      'required',
                'category_id'       =>      'required',
                'name'              =>      'required',
                'email'             =>      'required|email',
                'phone'             =>      'required',
        ]);

        $staff = Staff::find($id);

        $staff->location_id    =   $request->location_id;
        $staff->category_id    =   $request->category_id;
        $staff->name           =   $request->name;
        $staff->email          =   $request->email;
        $staff->phone          =   $request->phone;

        $staff->save();
        return redirect()->back()->with('success','Staff Information Update Successfully.');

    }





}
