<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffCategoryController extends Controller
{
    public function index() {
        $locations = Location::all();
        return view('backend.staffcategory',compact('locations'));
    }

    //Save Staff Category
    public function storeStaffCategory(Request $request)
    {
        $this->validate($request, [
            'location_id'   =>  'required',
            'name'           =>  'required'
        ]);

        DB::table('staff_category')->insert([
            'location_id'   =>  $request->location_id,
            'name'          =>  $request->name
        ]);

        return redirect()->back()->with('success','Staff category add successfully.');
    }


    //Get List of Staff Category
    public function getStaffCategory()
    {
        $staffcats =   DB::table('staff_category')
                        ->join('locations', 'staff_category.location_id', '=', 'locations.id')
                        ->select('staff_category.*','locations.clinic_name')
                        ->get();
        $locations = Location::all();
        return view('backend.staffcategoryall',compact('staffcats','locations'));
    }


    //Delete Staff Category
    public function delStaffCat($id) 
    {
        DB::table('staff_category')->where('id',$id)->delete();
        return redirect()->back()->with('success','Staff Category Delete Successfully.');
    }

    //Update Staff Category
    public function updateStaffCat(Request $request,$id)
    {
        $this->validate($request, [
            'location_id'   =>  'required',
            'name'           =>  'required'
        ]);
        
        DB::table('staff_category')
                ->where('id',$id)
                ->update([
                    'location_id'   =>  $request->location_id,
                    'name'          => $request->name,
                ]);
                
        return redirect()->back()->with('success','Staff Category Name Update Successfully.');
    }


}
