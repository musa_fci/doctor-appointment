<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommonController extends Controller
{
    //Get Hospital/Clinic List
    public function getLocation()
    {
        return DB::table('locations')->get();
    }


    //Get User Access Modules List
    public function getRole()
    {
        return  DB::table('modules')
                    ->join('module_wise_role', 'modules.id', '=', 'module_wise_role.module_id')
                    ->select('modules.*', 'module_wise_role.*')
                    ->get();
    }

    //Get Country List
    public function getCountry()
    {
        return  DB::table('countries')->get();
    }

    //Get State List
    public function getState($id)
    {
        return DB::table('states')->where('country_id',$id)->get();
    }

    //Get City List
    public function getCity($id)
    {
        return DB::table('cities')->where('state_id',$id)->get();
    }


    //Get Location/Clinic Wise Physician
    public function getLocationWisePhysician($id)
    {
        $physicians = DB::table('physicians')->where('location_id',$id)->get();

        return response()->json([
            'data'   => $physicians,
        ]);

    }

    //Get Country Wise State
    public function getCountryWiseState($id)
    {
        $states = DB::table('states')->where('country_id',$id)->get();

        return response()->json([
            'data'  =>  $states
        ]);
    }

    //Get Stare Wise City
    public function getcityWiseState($id)
    {
        $cities = DB::table('cities')->where('state_id',$id)->get();
        return response()->json([
            'data'  =>  $cities
        ]);
    }

}
