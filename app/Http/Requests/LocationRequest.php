<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clinic_name'           =>  'required',
            'email'                 =>  'required|email|unique:locations',            
            'password'              =>  'required|min:6',
            'phone'                 =>  'required',
            'website'               =>  'required',
            'country'               =>  'required',
            'state'                 =>  'required',
            'city'                  =>  'required',
            'address'               =>  'required',
            'post_code'             =>  'required',
            'controlled_license'    =>  'required',
            'reg_no'                =>  'required',
            'profile'               =>  'required',
        ];
    }
}
