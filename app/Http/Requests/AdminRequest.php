<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required',
            'email'         =>  'required|email|unique:admins',
            'username'      =>  'required|unique:admins',
            'password'      =>  'required|confirmed|min:6',
            'phone'         =>  'required',
            'website'       =>  'required',
            'country'       =>  'required',
            'state'         =>  'required',
            'city'          =>  'required',
            'address'       =>  'required',
            'timezone'      =>  'required',
            'post_code'     =>  'required',
            'reg_no'        =>  'required',
            'profile'       =>  'required',
        ];
    }
}
