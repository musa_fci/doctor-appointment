<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_id'           =>  'required',
            'physician_id'          =>  'required',
            'f_name'                =>  'required',
            'l_name'                =>  'required',
            'email'                 =>  'required|email|unique:patients',
            'country_code'          =>  'required',
            'phone_number'          =>  'required',
            'password'              =>  'required|confirmed|min:6',
            'address'               =>  'required',
            'country'               =>  'required',
            'state'                 =>  'required',
            // 'city'                  =>  'required',
            'state_id'              =>  'required',
            'zip_code'              =>  'required',
            'dob'                   =>  'required',
            'gender'                =>  'required',
            'initial_visit'         =>  'required',
            'expiration'            =>  'required',
            'caregiver'             =>  'required',
            'internal'              =>  'required',
            'social_security_no'    =>  'required',
            'rec_type'              =>  'required',
            'guardian'              =>  'required',
            'contact_option'        =>  'required',
            'referred_by'           =>  'required',
            'mmu_id'                =>  'required',
            // 'medical_symtomps'      =>  'required',
            // 'significant_issue'     =>  'required',
            // 'medical_info'          =>  'required',
        ];
    }


    public function messages()
    {
        return [
            'location_id.required'              =>  'The Location / Clinic Field is Required',
            'physician_id.required'             =>  'The Physician Field is Required',
            'f_name.required'                   =>  'The First Name Field is Required',
            'l_name.required'                   =>  'The Last Name Field is Required',
            'email.required'                    =>  'The Email Field is Required',
            'country_code.required'             =>  'The Country Code is Required',
            'phone_number.required'             =>  'The Phone Number Field is Required',
            'password.required'                 =>  'The Password Field is Required',
            'password.confirmed'                =>  'The Password Confirmation Does not Matched',
            'address.required'                  =>  'The Address Field is Required',
            'country.required'                  =>  'The Country Field is Required',
            'state.required'                    =>  'The State Field is Required',
            // 'city.required'                     =>  'The City Field is Required',
            'state_id.required'                 =>  'The State ID Field is Required',
            'zip_code.required'                 =>  'The Zip-Code Field is Required',
            'dob.required'                      =>  'The Date of Birth Field is Required',
            'gender.required'                   =>  'The Gender Field is Required',
            'initial_visit.required'            =>  'The Initial Visit Field is Required',
            'expiration.required'               =>  'The Expiration Field is Required',
            'caregiver.required'                =>  'The Caregiver Field is Required',
            'internal.required'                 =>  'The Internal Field is Required',
            'social_security_no.required'       =>  'The Social Security No Field is Required',
            'rec_type.required'                 =>  'The Recommendation Type Field is Required',
            'guardian.required'                 =>  'The Guardian Field is Required',
            'contact_option.required'           =>  'The Contact Option Field is Required',
            'referred_by.required'              =>  'The Referred By Field is Required',
            'mmu_id.required'                   =>  'The MMU ID Field is Required',
            // 'medical_symtomps.required'         =>  'Select Some of Your Medical Symtomps',
            // 'significant_issue.required'        =>  'Please Answer of Significant Issue',
            // 'medical_info.required'             =>  'You Must Need Select a Option',
        ];
    }
}
