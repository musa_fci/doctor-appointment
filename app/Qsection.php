<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qsection extends Model
{
    public function questions(){
        return $this->hasMany('App\Question','section_id');
    }
}
