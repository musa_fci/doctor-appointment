<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('admin_id');
            $table->string('clinic_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');
            $table->string('website');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('address');
            $table->string('post_code');
            $table->string('controlled_license');
            $table->string('reg_no');
            $table->string('profile');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
