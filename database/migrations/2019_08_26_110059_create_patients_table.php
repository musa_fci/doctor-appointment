<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('location_id');
            $table->unsignedBigInteger('physician_id');
            $table->string('f_name');
            $table->string('l_name');
            $table->string('email');
            $table->string('country_code');
            $table->string('phone_number');
            $table->string('password');
            $table->string('address');
            $table->string('country');
            $table->string('state');
            $table->string('city')->nullable();
            $table->string('state_id');
            $table->string('zip_code');
            $table->string('dob');
            $table->string('gender');
            $table->string('initial_visit');
            $table->string('expiration');
            $table->string('caregiver');
            $table->string('internal');
            $table->string('social_security_no');
            $table->string('rec_type');
            $table->string('guardian');
            $table->string('contact_option');
            $table->string('referred_by');
            $table->string('mmu_id');
            $table->string('state_id_front')->nullable();
            $table->string('state_id_back')->nullable();
            $table->string('patient_photo_id')->nullable();
            $table->string('patient_note')->nullable();
            $table->string('signature_name')->nullable();
            $table->string('signature_data')->nullable();
            $table->string('authy_id')->nullable();
            $table->boolean('verified')->default(false);
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
