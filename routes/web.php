<?php

//Patient Route
Route::resource('/patient','PatientController');
Route::get('/','PatientController@patientSignIn')->name('patientSignIn');
Route::post('/patientLogin','PatientController@patientLogin');


Auth::routes();


//Admin Route
// Route::resource('/admin','AdminController');
Route::get('/admin','AdminController@loginPageView')->name('login');
Route::post('/login','AdminController@login');



Route::group(['middleware' => ['auth:patient']], function () {
       //Frontend
       Route::get('/patientprofile','PatientController@patientprofile')->middleware('patient');
    
       Route::get('/afterVerify',function() {
           return view('frontend.afterVerify');
       });
   
       Route::get('/verifyPatient','PatientController@verifyPatient');
       Route::get('/verify','PatientController@verify'); 
});



Route::group(['prefix' => 'admin','middleware' => ['auth:admin']], function () {

    //Admin Dashboard
    Route::get('/dashboard', function () {
        return view('backend.dashboard');
    });

    //Location Route
    Route::resource('/location','LocationController');
    Route::get('/locationlist','LocationController@getAllLocations');
    Route::get('/deletelocation/{id}','LocationController@deleteLocation');
    Route::post('/updatelocation/{id}','LocationController@updateLocation');

    //Patient
    Route::post('/getQuestion','PatientController@getQuestion');

    Route::get('/patientadd',function(){
        return view('patientadd');
    });

    Route::get('/patientaddbyadmin','PatientController@patientaddByAdminView');
    Route::post('/patientaddbyadmin','PatientController@patientaddByAdmin');
    Route::get('/patientlist','PatientController@getAllPatients');
    Route::get('/deletepatient/{id}','PatientController@deletePatient');
    Route::get('/updatepatient/{id}','PatientController@updatePatient');


    
    Route::get('/downloadPatientInfo','PatientController@downloadPatientInfo');
    Route::get('/downloadPatientInfo/{id}','PatientController@downloadPatientInfoIDWise');



    //Physician Route
    Route::resource('/physician','PhysicianController');
    Route::get('/physicianlist','PhysicianController@getAllPhysicians');
    Route::get('/deletephysician/{id}','PhysicianController@deletePhysician');
    Route::post('/updatephysician/{id}','PhysicianController@updatePhysician');


    //Staff Route
    Route::resource('/staff','StaffController');
    Route::get('/stafflist','StaffController@getAllStaffs');
    Route::get('/deletestaff/{id}','StaffController@deleteStaff');
    Route::post('/updatestaff/{id}','StaffController@updateStaff');




    //Modules Route
    Route::get('/module','ModuleController@index');
    Route::post('/module','ModuleController@storeModule');
    Route::get('/modulelist','ModuleController@getAllModules');
    Route::get('/deletemodule/{id}','ModuleController@delModule');
    Route::post('/updatemodule/{id}','ModuleController@updateModule');


    Route::post('/getModule','ModuleController@getModule');
    Route::post('/delModule/{id}','ModuleController@delModule');
    Route::post('/getModuleById/{id}','ModuleController@getModuleById');
    Route::post('/updateModule/{id}','ModuleController@updateModule');


    Route::get('/modulewiserole','ModuleController@moduleWiseRoleView');
    Route::post('/modulewiserole','ModuleController@storeModuleWiseRole');
    Route::get('/modulewiserolelist','ModuleController@getModuleWiseRole');
    Route::get('/deletemodulewiserole/{id}','ModuleController@delModuleWiseRole');
    Route::post('/updateModuleWiseRole/{id}','ModuleController@updateModuleWiseRole');


    //Staff Category Route
    Route::get('/staffcategory','StaffCategoryController@index');
    Route::post('/staffcategory','StaffCategoryController@storeStaffCategory');
    Route::get('/staffcategorylist','StaffCategoryController@getStaffCategory');
    Route::get('/deletestaffcat/{id}','StaffCategoryController@delStaffCat');
    Route::post('/updatestaffcat/{id}','StaffCategoryController@updateStaffCat');


    //Section
    Route::resource('/qsection','QsectionController');
    Route::get('/sectionlist','QsectionController@getAllSections');
    Route::get('/deletesection/{id}','QsectionController@deleteSection');
    Route::post('/updatesection/{id}','QsectionController@updateSection');


    //Question
    Route::resource('/question','QuestionController');
    Route::get('/questionlist','QuestionController@getAllQuestions');
    Route::get('/deletequestion/{id}','QuestionController@deleteQuestion');
    Route::post('/updatequestion/{id}','QuestionController@updateQuestion');


    Route::post('/getQuestionType/{id}','QuestionController@getQuestionType');
    Route::post('/getQuestion/{id}','QuestionController@getQuestion');


    //Option
    Route::resource('/option','OptionController');


    //Common
    Route::post('/getLocation','CommonController@getLocation');
    Route::post('/getRole','CommonController@getRole');
    Route::post('/getCountry','CommonController@getCountry');
    Route::post('/getState/{id}','CommonController@getState');
    Route::post('/getCity/{id}','CommonController@getCity');    


});


Route::get('/getLocWisePhyi/{id}','CommonController@getLocationWisePhysician')->name('getLocWisePhyi');
Route::get('/getCountryWiseState/{id}','CommonController@getCountryWiseState')->name('getCountryWiseState');
Route::get('/getcityWiseState/{id}','CommonController@getcityWiseState')->name('getcityWiseState');