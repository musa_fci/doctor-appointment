<?php

// use PragmaRX\Countries\Package\Countries;


// https://www.larablocks.com/package/ibonly/laravel-accountkit
// https://github.com/ibonly/laravel-accountkit
// https://www.twilio.com/docs/authy/tutorials/account-verification-php-laravel


//https://github.com/mshoaibdev/laravel-country-states-city-seeds-migration/tree/master/database
    // $data = session()->all();
    // dd($user = Auth::user());

    // $countries = new Countries();

    // $all = $countries->all();
    // dd($all);

//Test Route
Route::get('/test',function(){

    // $data = session()->all();
    // dd($data);

    dd(Auth::user()->l_name);

    // if(Auth::user()){
    //     echo Auth::user()->id;
    // }

});



// Route::get('/',function(){
//     return view('frontend/signin');
// });
// Route::get('/signup',function(){
//     return view('frontend/signup');
// });



//Patient Route
Route::resource('/patient','PatientController');
Route::get('/','PatientController@patientSignIn')->name('patientSignIn');
Route::post('/patientLogin','PatientController@patientLogin');
// Route::get('/patientprofile','PatientController@patientprofile');








// Route::get('/verifyPatient',function(){
//     return view('frontend.verifyPatient');
// });


Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');


//Admin Route
Route::resource('/admin','AdminController');
Route::get('/login','AdminController@loginPageView')->name('login');
Route::post('/login','AdminController@login');



Route::group(['middleware' => ['auth']], function () {

    //Frontend
    Route::get('/patientprofile','PatientController@patientprofile');

    
    Route::get('/afterVerify',function() {
        return view('frontend.afterVerify');
    });


    Route::get('/verifyPatient','PatientController@verifyPatient');
    Route::get('/verify','PatientController@verify');


    //Admin Dashboard
    Route::get('/Dashboard', function () {
        return view('dashboard');
    });

    //Location Route
    Route::resource('/location','LocationController');


    //Physician Route
    Route::resource('/physician','PhysicianController');

    //Staff Route
    Route::resource('/staff','StaffController');

    //Modules Route
    Route::post('/module','ModuleController@storeModule');
    Route::post('/getModule','ModuleController@getModule');
    Route::post('/delModule/{id}','ModuleController@delModule');
    Route::post('/getModuleById/{id}','ModuleController@getModuleById');
    Route::post('/updateModule/{id}','ModuleController@updateModule');

    Route::post('/modulewiserole','ModuleController@storeModuleWiseRole');
    Route::post('/getModuleWiseRole','ModuleController@getModuleWiseRole');
    Route::post('/delModuleWiseRole/{id}','ModuleController@delModuleWiseRole');
    Route::post('/updateModuleWiseRole/{id}','ModuleController@updateModuleWiseRole');


    //Staff Category Route
    Route::post('/staffcategory','StaffCategoryController@storeStaffCategory');
    Route::post('/getStaffCategory','StaffCategoryController@getStaffCategory');
    Route::post('/delStaffCat/{id}','StaffCategoryController@delStaffCat');
    Route::post('/updateStaffCat/{id}','StaffCategoryController@updateStaffCat');


    //Common
    Route::post('/getLocation','CommonController@getLocation');
    Route::post('/getRole','CommonController@getRole');
    Route::post('/getCountry','CommonController@getCountry');
    Route::post('/getState/{id}','CommonController@getState');
    Route::post('/getCity/{id}','CommonController@getCity');


});


Route::get('/getLocWisePhyi/{id}','CommonController@getLocationWisePhysician')->name('getLocWisePhyi');
Route::get('/getCountryWiseState/{id}','CommonController@getCountryWiseState')->name('getCountryWiseState');
Route::get('/getcityWiseState/{id}','CommonController@getcityWiseState')->name('getcityWiseState');