
$(function () {

    $('#forNew').css("display", "block");
    $('#forExisting').css("display", "none");

    $("#existingPatient").click(function() {
        $('#forExisting').css("display", "block");
        $('#forNew').css("display", "none");
    });

    $("#newPatient").click(function() {
        $('#forExisting').css("display", "none");
        $('#forNew').css("display", "block");
    });

    //============================================

    var canvas = document.getElementById("signature");
    var signaturePad = new SignaturePad(canvas);

    $("canvas").on("mouseenter", function () {
        $(".ic-your-sig").hide();
    });

    $('#clear-signature').on('click', function () {
        signaturePad.clear();
    });

    $('#clear-signature').on('click', function () {
        $(".ic-your-sig").show();
    });
});