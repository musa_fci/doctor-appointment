<!DOCTYPE html>
<html lang="en-us">

    @include('backend._partial._head')

    <body class="">
        
        @include('backend._partial._header')

        @include('backend._partial._leftpanel')


            @section('content')
            @show



        @include('backend._partial._footer')
        @include('backend._partial._shortcut')


        @include('backend._partial._script')

    </body>

</html>