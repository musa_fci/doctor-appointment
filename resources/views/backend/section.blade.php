@extends('backend.app')

@section('content')
    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <span class="ribbon-button-alignment"> 
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Dashboard</li>
                <li>Patient Setting</li>
                <li>Add Section</li>
            </ol>
            <!-- end breadcrumb -->

        </div>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">
							<i class="fa fa-edit fa-fw "></i> 
								Basic Setup 
							<span>> 
								Add Section                                
							</span>
						</h1>                        
                </div>
            </div>


            <!-- widget grid -->
            <section id="widget-grid" class="">

                <!-- START ROW -->

                <div class="row">

                    <!-- NEW COL START -->
                    <article class="col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                           
                            <header>
                                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                                <h2>Add Section form</h2>
                            </header>

                            <!-- widget div-->
                            <div>

                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <form action="" method="POST" class="smart-form">

                                        @csrf

                                        <div class="smart-form">

                                            @if(session()->has('success'))
                                                <header class="admin_success_msg">
                                                    {{ session()->get('success') }}
                                                </header>
                                            @endif


                                            <fieldset>
                                                <section>
                                                    <label class="label">Section Title</label>
                                                    <label class="input">
                                                        <input type="text" name="title" class="input-sm">
                                                    </label>
                                                    @if ($errors->has('title'))
                                                        <p class="text-danger">{{ $errors->first('title') }}</p>
                                                    @endif
                                                </section>

                                                <section>
                                                    <label class="label">Section Sub-Title</label>
                                                    <label class="input">
                                                        <input type="text" name="sub_title" class="input-sm">
                                                    </label>                                                    
                                                </section>

                                                <section>
                                                    <label class="label">Description</label>
                                                    <label class="input">
                                                        <input type="text" name="description" class="input-sm">
                                                    </label>                                                    
                                                </section>
                                                
                                                <section>
                                                    <label class="select">Placement Step
                                                    <select name="placement_step">
                                                        <option value="0" selected="" disabled="">Placement Step</option>                                                    
                                                        <option value="1">Placement Step 1</option>                                                    
                                                        <option value="2">Placement Step 2</option>                                                    
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                    </label>                                                    
                                                </section>

                                                <section>
                                                    <label class="select">Placement Order
                                                    <select name="placement_order">
                                                        <option value="0" selected="" disabled="">Placement Order</option>                                                    
                                                        <option value="1">Placement Order 1</option>                                                    
                                                        <option value="2">Placement Order 2</option>                                                    
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                    </label>                                                    
                                                </section>

                                                <section>
                                                    <label class="label">Is Required ?</label>
                                                    <div class="row">
                                                        <div class="col col-4">
                                                            <label class="radio">
                                                                <input type="radio" name="is_required" value="1">
                                                                <i></i>Yes</label>
                                                            <label class="radio">
                                                                <input type="radio" name="is_required" value="0" checked="checked">
                                                                <i></i>No</label>                                                                
                                                        </div>                                                    
                                                    </div>
                                                </section>                             
                                            </fieldset>

                                            <footer>
                                                <button type="submit" class="btn btn-primary">
                                                    Submit
                                                </button>
                                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                                    Back
                                                </button>
                                            </footer>

                                        </div>
                                    </form>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>
                    <!-- END COL -->



                </div>

                <!-- END ROW -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->
@endsection