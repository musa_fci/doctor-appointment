@extends('backend.app')

@section('content')

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li><li>Basic Setup</li><li>Question Data Table</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i> 
                        Basic Setup
                    <span>> 
                        Question Table
                    </span>
                </h1>
            </div>
        </div>
        
        <!-- widget grid -->
        <section id="widget-grid" class="">
        
            <!-- row -->
            <div class="row">
        
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Question Data Table</h2>            
                        </header>                        
        
                        <!-- widget div-->
                        <div>
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                @if(session()->has('success'))
                                    <header class="admin_success_msg">
                                        {{ session()->get('success') }}
                                    </header>
                                @endif
        
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th data-hide="phone">SL</th>
                                            <th data-class="expand">Section</th>
                                            <th data-class="expand">Question Type </th>
                                            <th data-class="expand">Question </th>                                            
                                            <th data-hide="phone">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($questions))
                                            @foreach($questions as $key=>$question)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $question->section_title }}</td>
                                                    @if($question->question_type == 1)
                                                        <td>Checkbox</td>
                                                    @elseif($question->question_type == 2)
                                                        <td>Input Field</td>
                                                    @else
                                                        <td>Radio Button</td>
                                                    @endif
                                                    <td>{{ $question->question }} </td>                                                
                                                    <td>
                                                        <a href="{{ URL::to('/admin/deletequestion/' . $question->id) }}" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-danger">Delete</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->
        
                        </div>
                        <!-- end widget div -->
        
                    </div>
                    <!-- end widget -->
        
                </article>
                <!-- WIDGET END -->
        
            </div>
        
            <!-- end row -->
        
        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

@endsection