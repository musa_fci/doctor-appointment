@extends('backend.app')

@section('content')
    <!-- MAIN PANEL -->
<div id="main" role="main">

<!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment"> 
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Dashboard</li>
        <li>Patient</li>
        <li>Patient Add</li>
    </ol>
    <!-- end breadcrumb -->
</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-edit fa-fw "></i> 
                    Patient 
                <span>> 
                    Add Patient                                
                </span>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- START ROW -->

        <div class="row">

            <!-- NEW COL START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Add Patient form</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            @if(session()->has('success'))
                                <header class="admin_success_msg">
                                    {{ session()->get('success') }}
                                </header>
                            @endif

                            <div class="smart-form">

                                <form action="{{ URL::to('/admin/patientaddbyadmin') }}" method="POST" enctype="multipart/form-data" class="smart-form">

                                @csrf

                                <header>
                                    Patient General Information
                                </header>                                

                                <fieldset>

                                    <section>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="select">Clinic/Location
                                                    <select id="location" name="location_id">
                                                        <option value="0" selected disabled>Select Location</option>
                                                        @if(count($locations)) 
                                                            @foreach ($locations as $location)
                                                            <option value="{{ $location->id }}">{{$location->clinic_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('location_id'))
                                                    <p class="text-danger">{{ $errors->first('location_id') }}</p>
                                                @endif
                                            </section>

                                            <section class="col col-6">
                                                <label class="select">Physician
                                                    <select id="physician" name="physician_id">
                                                        <option value="0" selected disabled>Select Physician</option>

                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('physician_id'))
                                                    <p class="text-danger">{{ $errors->first('physician_id') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">First Name</label>
                                                <label class="input">
                                                    <input type="text" name="f_name">
                                                </label>
                                                @if ($errors->has('f_name'))
                                                    <p class="text-danger">{{ $errors->first('f_name') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Last Name</label>
                                                <label class="input">
                                                    <input type="text" name="l_name" >
                                                </label>
                                                @if ($errors->has('l_name'))
                                                    <p class="text-danger">{{ $errors->first('l_name') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Email</label>
                                                <label class="input">
                                                    <input type="email" name="email" class="input-sm">
                                                </label>
                                                @if ($errors->has('email'))
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-4">
                                                <label class="select">Country Code
                                                <select name="country_code" id="country_code">
                                                    <option value="0" selected disabled>Select Code</option>
                                                    @if(count($countries)) 
                                                        @foreach ($countries as $country)
                                                        <option value="{{ $country->phonecode }}">{{$country->country_name}} - {{ $country->phonecode }}</option>
                                                        @endforeach 
                                                    @endif
                                                </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('country_code'))
                                                    <p class="text-danger">{{ $errors->first('country_code') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Phone Number</label>
                                                <label class="input">
                                                    <input type="text" name="phone_number" class="input-sm">
                                                </label>
                                                @if ($errors->has('phone_number'))
                                                    <p class="text-danger">{{ $errors->first('phone_number') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Password</label>
                                                <label class="input">
                                                    <input type="password" name="password" class="input-sm">
                                                </label>
                                                @if ($errors->has('password'))
                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                @endif
                                            </section>

                                            <section class="col col-6">
                                                <label class="label">Confirm Password</label>
                                                <label class="input">
                                                    <input type="password" name="password_confirmation" class="input-sm">
                                                </label>
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-12">
                                                <label class="label">Address</label>
                                                <label class="input">
                                                    <input type="text" name="address" class="input-sm">
                                                </label>
                                                @if ($errors->has('address'))
                                                    <p class="text-danger">{{ $errors->first('address') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="select">Country
                                                    <select name="country" id="country">
                                                        <option value="0" selected disabled>Select Country</option>
                                                        @if(count($countries)) 
                                                            @foreach ($countries as $country)
                                                            <option value="{{ $country->id }}">{{$country->country_name}}</option>
                                                            @endforeach 
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('country'))
                                                    <p class="text-danger">{{ $errors->first('country') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-4">
                                                <label class="select">State
                                                    <select name="state" id="state">
                                                        <option value="0" selected disabled>State</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('state'))
                                                    <p class="text-danger">{{ $errors->first('state') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-4">
                                                <label class="select">City
                                                    <select name="city" id="city">
                                                        <option value="0" selected disabled>City</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('city'))
                                                    <p class="text-danger">{{ $errors->first('city') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">State ID</label>
                                                <label class="input">
                                                    <input type="text" name="state_id" class="input-sm">
                                                </label>
                                                @if ($errors->has('state_id'))
                                                    <p class="text-danger">{{ $errors->first('state_id') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">ZIP Code</label>
                                                <label class="input">
                                                    <input type="text" name="zip_code" class="input-sm">
                                                </label>
                                                @if ($errors->has('zip_code'))
                                                    <p class="text-danger">{{ $errors->first('zip_code') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="label">Date of Birth</label>
                                                <label class="input">
                                                    <input type="date" name="dob" class="input-sm">
                                                </label>
                                                @if ($errors->has('dob'))
                                                    <p class="text-danger">{{ $errors->first('dob') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="select">Gender
                                                    <select name="gender">
                                                        <option value="0" selected="" disabled="">Gender</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('gender'))
                                                    <p class="text-danger">{{ $errors->first('gender') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Initial Visit</label>
                                                <label class="input">
                                                    <input type="date" name="initial_visit" class="input-sm">
                                                </label>
                                                @if ($errors->has('initial_visit'))
                                                    <p class="text-danger">{{ $errors->first('initial_visit') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Expiration</label>
                                                <label class="input">
                                                    <input type="date" name="expiration" class="input-sm">
                                                </label>
                                                @if ($errors->has('expiration'))
                                                    <p class="text-danger">{{ $errors->first('expiration') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="label">Caregiver</label>
                                                <label class="input">
                                                    <input type="text" name="caregiver" class="input-sm">
                                                </label>
                                                @if ($errors->has('caregiver'))
                                                    <p class="text-danger">{{ $errors->first('caregiver') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Internal #</label>
                                                <label class="input">
                                                    <input type="text" name="internal" class="input-sm">
                                                </label>
                                                @if ($errors->has('internal'))
                                                    <p class="text-danger">{{ $errors->first('internal') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Social Security Number</label>
                                                <label class="input">
                                                    <input type="password" name="social_security_no" class="input-sm">
                                                </label>
                                                @if ($errors->has('social_security_no'))
                                                    <p class="text-danger">{{ $errors->first('social_security_no') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="select">Rec Type
                                                    <select name="rec_type">
                                                        <option value="0" selected="" disabled="">Rec Type</option>
                                                        <option value="Recommendation One">Recommendation One</option>
                                                        <option value="Recommendation Two">Recommendation Two</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('rec_type'))
                                                    <p class="text-danger">{{ $errors->first('rec_type') }}</p>
                                                @endif
                                            </section>
                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="select">Parent / Guardian
                                                    <select name="guardian">
                                                        <option value="0" selected="" disabled="">Parent / Guardian</option>
                                                        <option value="Father">Father</option>
                                                        <option value="Mother">Mother</option>
                                                        <option value="Brother">Brother</option>
                                                        <option value="Sister">Sister</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('guardian'))
                                                    <p class="text-danger">{{ $errors->first('guardian') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="select">Referred By
                                                    <select name="referred_by">
                                                        <option value="0" selected="" disabled="">Referred By</option>
                                                        <option value="Google">Google</option>
                                                        <option value="Facebook">Facebook</option>
                                                        <option value="Yahoo">Yahoo</option>
                                                        <option value="Twitter">Twitter</option>
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('referred_by'))
                                                    <p class="text-danger">{{ $errors->first('referred_by') }}</p>
                                                @endif
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">MMU ID #</label>
                                                <label class="input">
                                                    <input type="text" name="mmu_id" class="input-sm">
                                                </label>
                                                @if ($errors->has('mmu_id'))
                                                    <p class="text-danger">{{ $errors->first('mmu_id') }}</p>
                                                @endif
                                            </section>
                                            <section>
                                                <label class="label">Contact Options</label>
                                                <div class="col col-3">
                                                    <label class="radio">
                                                        <input type="radio" name="contact_option" value="Email" checked="checked">
                                                        <i></i>Email</label>
                                                    <label class="radio">
                                                        <input type="radio" name="contact_option" value="SMS">
                                                        <i></i>SMS</label>
                                                    @if ($errors->has('contact_option'))
                                                        <p class="text-danger">{{ $errors->first('contact_option') }}</p>
                                                    @endif
                                                </div>
                                            </section>
                                        </div>
                                    </section>

                                </fieldset>

                                <header>
                                    Patient Questions Section (Optional)
                                </header>

                                <fieldset>
                                    <section class="question_system">
                                        <div class="row">
                                        @if(count($sections))
                                            @foreach($sections as $key=>$section)
                                                <div class="col col-12">
                                                    <div class="question_section_part">
                                                        <h3>{{ $section->title }}</h3>
                                                        <h5>{{ $section->sub_title }}</h5>
                                                        <p>{{ $section->description }}</p>
                                                    </div>
                                                    <div class="question_question_part">

                                                        <!-- Loop for Checkbox -->
                                                        @foreach($section->questions as $question)
                                                            @if($question->question_type == 1)
                                                                <div>
                                                                    <input type="hidden" name="symtomps_question_id[{{ $question->id }}]" value="{{ $question->id }}">
                                                                    <!-- <h3>{{ $question->title }}</h3>
                                                                    <h5>{{ $question->sub_title }}</h5>
                                                                    <h6>{{ $question->description }}</h6> -->
                                                                    <p>{{ $question->question }}</p>

                                                                    <div class="row">
                                                                    @foreach($question->options as $option)
                                                                        <div class="col col-3">                                                                            
                                                                                <label class="checkbox">
                                                                                <input type="checkbox" value="{{ $option->id }}" name="medical_symtomps[{{ $option->id }}]">
                                                                                    <i></i>{{ $option->option }}
                                                                                </label>                                                                            
                                                                        </div>
                                                                        @endforeach
                                                                    </div>                                                        
                                                                </div>
                                                            @endif
                                                        @endforeach


                                                        <!-- Loop for Input Field -->
                                                        @foreach($section->questions as $question)
                                                        @if($question->question_type == 2)
                                                            <div class="row">
                                                                <!-- <h3>{{ $question->title }}</h3>
                                                                    <h5>{{ $question->sub_title }}</h5>
                                                                    <h6>{{ $question->description }}</h6> -->

                                                                @foreach($question->options as $option)
                                                                    <section class="col col-12">
                                                                        <p>{{ $question->question }}</p>
                                                                        <label class="input">
                                                                            <input type="hidden" name="significant_question_id[{{ $question->id }}]" value="{{ $question->id }}">
                                                                            <input type="text" name="significant_issue[{{ $option->id }}]" placeholder="Enter Text Here" class="form-control">
                                                                        </label>                                                                        
                                                                    </section>
                                                                @endforeach                                                               
                                                            </div>
                                                            @endif
                                                        @endforeach

                                                        <!-- Loop for Radio Button -->
                                                        @foreach($section->questions as $question)
                                                        @if($question->question_type == 3)
                                                            <div class="row">
                                                                @foreach($question->options as $option)
                                                                    <div class="col-lg-12">                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-6 col-md-4 col-lg-4 ic-profile-round-radio">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <p>{{ $question->question }}</p>
                                                                                        <input type="hidden" name="medical_info_question_id[{{ $question->id }}]" value="{{ $question->id }}">
                                                                                    </div>
                                                                                    <div class="col-lg-12">
                                                                                        <label class="radio">
                                                                                            <input type="radio" value="1" name="medical_info[{{$question->id}}]">
                                                                                            <i></i>Yes
                                                                                        </label>
                                                                                        <label class="radio">
                                                                                            <input type="radio" value="0" checked name="medical_info[{{$question->id}}]">
                                                                                            <i></i>No
                                                                                        </label>
                                                                                        
                                                                                    </div>                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        </div>
                                    </section>
                                </fieldset>

                                <header>
                                    Drivers License / State ID And Photo (Optional)
                                </header>

                                <fieldset>
                                    <section>
                                        <div class="row">

                                            <section class="col col-3">
                                                <div class="ic-profile-upload">
                                                    <span>Drivers Lic / State ID Front</span>
                                                    <ul>
                                                        <li>
                                                            <label class="file-type">
                                                                Camera
                                                                <input type="button" onclick="callCamera()" style="display:none">
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="file-type">
                                                                Capture
                                                                <input type="button" onClick="takeSnapshotForStateIdFront()" style="display:none">
                                                            </label>
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" value="" name="state_id_front" id="state_id_front">
                                                </div>
                                            </section>

                                            <section class="col col-3">
                                                <div class="ic-profile-upload">
                                                    <span>Drivers Lic / State ID Back</span>
                                                    <ul>
                                                        <li>
                                                            <label class="file-type">
                                                                Camera
                                                                <input type="button" onclick="callCamera()" style="display:none">
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="file-type">
                                                                Capture
                                                                <input type="button" onClick="takeSnapshotForStateIdBack()" style="display:none">
                                                            </label>
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" value="" name="state_id_back" id="state_id_back">
                                                </div>
                                            </section>

                                            <section class="col col-3">
                                                <div class="ic-profile-upload">
                                                    <span>Patient Photo <br> Required for ID Card</span>
                                                    <ul>
                                                        <li>
                                                            <label class="file-type">
                                                                Camera
                                                                <input type="button" onclick="callCamera()" style="display:none">
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="file-type">
                                                                Capture
                                                                <input type="button" onClick="takeSnapshotForPatient()" style="display:none">
                                                            </label>
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" value="" name="patient_photo_id" id="patient_photo_id">
                                                </div>
                                            </section>

                                            <section class="col col-3" id="web_cam_show">

                                            </section>

                                        </div>
                                    </section>

                                    <section>
                                        <div class="row">
                                            <section class="col col-4 offset-md-4">
                                                <div id="webcam_snapshot"></div>
                                            </section>
                                        </div>
                                    </section>
                                </fieldset>

                                <header>
                                    Patient Files (Optional)
                                </header>

                                <fieldset>
                                    <section class="ic-patient-files">
                                        <div class="row">
                                            <div class="col col-6 text-center">
                                                <div class="ic-ptf-item">
                                                    <img src="http://localhost:8000/frontend/images/upload-icon.png" alt="Upload">
                                                    <h6>Upload files from here</h6>
                                                    <ul>
                                                        <li>
                                                            <label class="file-type">
                                                                Select Files
                                                                <input type="file" id="patient_file" name="patient_file[]" multiple="">
                                                            </label>                                                                    
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col col-6">
                                                <div class="ic-patient-form">
                                                    <ul class="ic-patient-head">
                                                        <li>
                                                            File Name
                                                        </li>
                                                    </ul>
                                                    <div class="ic-patient-body">
                                                        <h6 id="noFile">No patient files uploaded yet!</h6>
                                                        <ul class="list-group" id="showPatientFiles">                                                            
                                                            <!-- After upload file..file will be show here -->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>

                                <header>
                                    Physician / Stuff Signature (Optional)
                                </header>

                                <fieldset>
                                    <section>
                                        <div class="row">
                                            <div class="col col-12">
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <label class="label">Signees Name</label>
                                                        <label class="input">
                                                            <input type="text" name="signature_name" placeholder="Enter Text Here" class="form-control input-sm">
                                                        </label>

                                                    </div>

                                                    <div class="col col-12">
                                                        <div class="ic-signature">
                                                            <h6>Please sign below using a stylus, your mouse, or your finger and then click “SAVE”</h6>
                                                            <div class="ic-signature-inner">
                                                                <div class="ic-signature-canvas d-none d-sm-block d-md-block d-lg-block">
                                                                    <canvas id="signature" width="450" height="150" style="touch-action: none;"></canvas>
                                                                    <input type="hidden" value="" name="signature_data" id="signature_data">
                                                                </div>
                                                                <p>Save your signature before submit the form</p>
                                                                <button type="button" id="clear-signature" onClick="clearSignature()" class="small-btn mr-2">Clear</button>
                                                                <button type="button" class="small-btn" onclick="getSignature()">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="window.history.back();">
                                        Back
                                    </button>
                                </footer>
                                </form>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->

        </div>

        <!-- END ROW -->

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection


@section('script')
    
        <!-- Webcam Open & Capture -->
        <script src="{{ asset('frontend/js/webcam.min.js') }}"></script>

        <!-- For Signature -->
        <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>

        
        <!-- Configure a few settings and attach camera -->
        <script language="JavaScript">
            async function callCamera() {
                Webcam.set({
                    width: 320,
                    height: 240,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach( '#web_cam_show' );
            }
        </script>
            



        <!-- Code to handle taking the snapshot and displaying it locally -->
        <script language="JavaScript">
            function takeSnapshotForPatient() {
                // take snapshot and get image data
                Webcam.snap( function(data_uri) {
                    // display results in page
                    document.getElementById('webcam_snapshot').innerHTML = 
                        '<h2>Here is your image:</h2>' + 
                        '<img src="'+data_uri+'"/>';
                } );

                var photo_data = $('#webcam_snapshot img').attr('src');
                $('#patient_photo_id').val(photo_data);
                // console.log(photo_data);
            }


            function takeSnapshotForStateIdFront() {
                // take snapshot and get image data
                Webcam.snap( function(data_uri) {
                    // display results in page
                    document.getElementById('webcam_snapshot').innerHTML = 
                        '<h2>Here is your image:</h2>' + 
                        '<img src="'+data_uri+'"/>';
                } );

                var photo_data = $('#webcam_snapshot img').attr('src');
                $('#state_id_front').val(photo_data);
                // console.log(photo_data);
            }


            function takeSnapshotForStateIdBack() {
                // take snapshot and get image data
                Webcam.snap( function(data_uri) {
                    // display results in page
                    document.getElementById('webcam_snapshot').innerHTML = 
                        '<h2>Here is your image:</h2>' + 
                        '<img src="'+data_uri+'"/>';
                } );

                var photo_data = $('#webcam_snapshot img').attr('src');
                $('#state_id_back').val(photo_data);
                // console.log(photo_data);
            }
        </script>


        <script>
            //For Physician
            $('#location').on('change',function(e){
                var location_id = e.target.value;                
                $.ajax({
                    url: "{{url('/getLocWisePhyi')}}/"+location_id,
                    type: "GET",
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },                
                    success: function(data){

                        var physician =  $('#physician').empty();
                        physician.append("<option value='0' selected disabled>Select Physician</option>");
                        $.each(data.data,function(key,val){
                            physician.append('<option value ="'+val.id+'">'+val.name+'</option>');
                        });
                        
                    }
                });
            });


            //For Country Wise State
            $('#country').on('change',function(e){
                var country_id = e.target.value;
                $.ajax({
                    url: "{{url('/getCountryWiseState')}}/"+country_id,
                    type: "GET",
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        var state =  $('#state').empty();
                        state.append("<option value='0' selected disabled>State</option>");
                        $.each(data.data,function(key,val){
                            state.append('<option value ="'+val.id+'">'+val.state_name+'</option>');
                        });
                    }
                })
            });

            //For State Wise City
            $('#state').on('change',function(e){
                var city_id = e.target.value;

                $.ajax({
                    url: "{{url('/getcityWiseState')}}/"+city_id,
                    type: "GET",
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        var state =  $('#city').empty();
                        state.append("<option value='0' selected disabled>City</option>");
                        $.each(data.data,function(key,val){
                            state.append('<option value ="'+val.id+'">'+val.city_name+'</option>');
                        });
                    }
                })
            });
        </script>

        <!--signature-->
        <script>
            var canvas = document.getElementById('signature');

            // var canvas = document.querySelector("canvas");

            var signaturePad = new SignaturePad(canvas);

            // Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible parameters)
            signaturePad.toDataURL(); // save image as PNG
            signaturePad.toDataURL("image/jpeg"); // save image as JPEG
            signaturePad.toDataURL("image/svg+xml"); // save image as SVG

            // Draws signature image from data URL.
            // NOTE: This method does not populate internal data structure that represents drawn signature. Thus, after using #fromDataURL, #toData won't work properly.
            signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

            // Returns signature image as an array of point groups
            const data = signaturePad.toData();


            // Draws signature image from an array of point groups
            signaturePad.fromData(data);

            // Clears the canvas
            signaturePad.clear();

            // Returns true if canvas is empty, otherwise returns false
            signaturePad.isEmpty();

            // Unbinds all event handlers
            signaturePad.off();

            // Rebinds all event handlers
            signaturePad.on();


            //CUSTOM BY DEVELOPER
            function getSignature() {
                var canvas = document.getElementById('signature');
                var dataURL = canvas.toDataURL();
                $('#signature_data').val(dataURL);
                console.log(dataURL);
            }


            //Clear Signature Pad
                function clearSignature() {
                var canvas = document.getElementById('signature');
                var signaturePad = new SignaturePad(canvas);
                signaturePad.clear();
            }

        </script>



        <!--Selected Files Show in a Div-->
        <script>
            $(document).ready(function() {

                $("#patient_file").change(function() {
                    $("#noFile").hide();
                    $("#showPatientFiles").empty();
                    var names = [];
                    for (var i = 0; i < $(this).get(0).files.length; ++i) {
                        names.push($(this).get(0).files[i].name);
                    }

                    $(names).each(function (index, value) {                        
                        $("#showPatientFiles").append('<li class="list-group-item">'+value+'</li>');
                    });
                });

            });
        </script>

@endsection