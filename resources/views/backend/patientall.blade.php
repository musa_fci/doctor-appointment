@extends('backend.app')

@section('content')
    <!-- MAIN PANEL -->
<div id="main" role="main">

<!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment"> 
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Dashboard</li>
        <li>Patient</li>
        <li>All Patient</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i> 
                        Patient
                    <span>> 
                        All Patient
                    </span>
                </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>All Patient Table</h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            @if(session()->has('success'))
                                <header class="admin_success_msg">
                                    {{ session()->get('success') }}
                                </header>
                            @endif

                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-hide="phone">SL </th>
                                        <th data-class="expand">Photo </th>
                                        <th data-class="expand">Name </th>
                                        <th data-class="expand">Location </th>
                                        <th data-class="expand">Physician </th>
                                        <th data-class="expand">City </th>
                                        <th data-class="expand">Zip </th>
                                        <th data-class="expand">Init:visit </th>
                                        <th data-class="expand">Expires </th>
                                        <!-- <th data-class="expand">State ID </th> -->
                                        <th data-class="expand">DOB </th>
                                        <th data-class="expand">Email </th>
                                        <th data-hide="phone">Status</th>
                                        <th data-hide="phone">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($patients))
                                        @foreach($patients as $key=>$patient)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                @if($patient->patient_photo_id)
                                                    <td>
                                                        <img src="{{ asset('storage/images/'.$patient->patient_photo_id) }}" class="patient_id">
                                                    </td>
                                                @else
                                                <td>
                                                    <img src="{{ asset('storage/images/demo-patient.png') }}" class="patient_id">
                                                </td>
                                                @endif

                                                <td>{{ $patient->f_name }} {{ $patient->l_name }}</td>
                                                <td>{{ $patient->clinic_name }}</td>
                                                <td>{{ $patient->physician_name }}</td>
                                                <td>{{ $patient->city_name }}</td>
                                                <td>{{ $patient->zip_code }}</td>
                                                <td>{{ $patient->initial_visit }}</td>
                                                <td>{{ $patient->expiration }}</td>
                                                <!-- <td>{{ $patient->state_id }}</td> -->
                                                <td>{{ $patient->dob }}</td>
                                                <td>{{ $patient->email }}</td>
                                                @if($patient->verified == 1)
                                                <td>
                                                    <button type="button" class="btn btn-success">Approved</button>
                                                </td>
                                                @else
                                                <td>
                                                    <button type="button" class="btn btn-info">Pending</button>
                                                </td>
                                                @endif
                                                <td>
                                                    <a href="{{ action('PatientController@downloadPatientInfoIDWise',$patient->id) }}" class="btn btn-success">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                    <a href="{{ action('PatientController@deletePatient',$patient->id) }}"  onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ action('PatientController@updatePatient',$patient->id) }}" class="btn btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection