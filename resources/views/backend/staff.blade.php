@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li>
            <li>Staff</li>
            <li>Staff Add</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-edit fa-fw "></i> 
                            Staff 
                        <span>> 
                            Add Staff                                
                        </span>
                    </h1>
            </div>
        </div>


        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- START ROW -->

            <div class="row">

                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Add Staff form</h2>
                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <form action="" method="POST" class="smart-form">

                                    @csrf

                                    <div class="smart-form">

                                        @if(session()->has('success'))
                                            <header class="admin_success_msg">
                                                {{ session()->get('success') }}
                                            </header>
                                        @endif  

                                        <input type="hidden" name="admin_id" value="2" class="input-sm">

                                        <fieldset>

                                            <section>
                                                <label class="select">Select Clinic/Location
                                                    <select name="location_id">
                                                        <option value="0" selected disabled>Select Clinic/Location</option>
                                                        @if(count($locations)) 
                                                            @foreach ($locations as $location)
                                                            <option value="{{ $location->id }}">{{$location->clinic_name}}</option>
                                                            @endforeach 
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('location_id'))
                                                    <p class="text-danger">{{ $errors->first('location_id') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="select">Staff Category
                                                    <select name="category_id">
                                                        <option value="0" selected disabled>Staff Category</option>
                                                        @if(count($staffcats)) 
                                                            @foreach ($staffcats as $staffcat)
                                                            <option value="{{ $staffcat->id }}">{{$staffcat->name}}</option>
                                                            @endforeach 
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('category_id'))
                                                    <p class="text-danger">{{ $errors->first('category_id') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Staff Name</label>
                                                <label class="input">
                                                    <input type="text" name="name" class="input-sm">
                                                </label>
                                                @if ($errors->has('name'))
                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Email</label>
                                                <label class="input">
                                                    <input type="email" name="email" class="input-sm">
                                                </label>
                                                @if ($errors->has('email'))
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </section>
                                            
                                            <section>
                                                <label class="label">Password</label>
                                                <label class="input">
                                                    <input type="password" name="password" class="input-sm">
                                                </label>
                                                @if ($errors->has('password'))
                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Phone</label>
                                                <label class="input">
                                                    <input type="text" name="phone" class="input-sm">
                                                </label>
                                                @if ($errors->has('phone'))
                                                    <p class="text-danger">{{ $errors->first('phone') }}</p>
                                                @endif
                                            </section>                                                                                    

                                            <section>
                                                <label class="label">Notification</label>
                                                <div class="row">
                                                    <div class="col col-4">
                                                        <label class="radio">
                                                            <input type="radio" name="notification" value="1" checked="checked">
                                                            <i></i>Yes</label>
                                                        <label class="radio">
                                                            <input type="radio" name="notification" value="0">
                                                            <i></i>No</label>
                                                            @if ($errors->has('notification'))
                                                                <p class="text-danger">{{ $errors->first('notification') }}</p>
                                                            @endif
                                                    </div>                                                    
                                                </div>
                                            </section>

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">
                                                Back
                                            </button>
                                        </footer>
                                    </form>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- END COL -->



            </div>

            <!-- END ROW -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection