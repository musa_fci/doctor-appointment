
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
            
            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <img src="{{ asset('backend/img/avatars/sunny.png') }}" alt="me" class="online" /> 
                <span>
                    @if(Auth::guard('admin')->user())
                        {{ Auth::guard('admin')->user()->name }} 
                    @endif
                </span>
                <i class="fa fa-angle-down"></i>
            </a> 
            
        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive-->
    <nav>

        <ul>
            <li>
                <a href="{{ URL::to('/admin/dashboard') }}">
                    <i class="fa fa-lg fa-fw fa-home"></i> 
                    <span class="menu-item-parent">Dashboard</span>
                </a>
            </li>
    
            <li class="top-menu-invisible">
                <a href="#"><i class="fa fa-lg fa-fw fa-ambulance"></i> <span class="menu-item-parent">Location</span></a>
                <ul>
                    <li class="">
                        <a href="{{ URL::to('/admin/location') }}" title="Add Clinic/Location"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Clinic/Location</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/locationlist') }}" title="All Clinic/Location"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">All Clinic/Location</span></a>
                    </li>
                </ul>
            </li>

            <li class="top-menu-invisible">
                <a href="#"><i class="fa fa-lg fa-fw fa-wheelchair"></i> <span class="menu-item-parent">Patient</span></a>
                <ul>
                    <li class="">
                        <a href="{{ URL::to('/admin/patientaddbyadmin') }}" title="Add New Patient"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add New Patient</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/patientlist') }}" title="All Patient"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">All Patient</span></a>
                    </li>
                </ul>
            </li>

            <li class="top-menu-invisible">
                <a href="#"><i class="fa fa-lg fa-fw fa-user-md"></i> <span class="menu-item-parent">Physician</span></a>
                <ul>
                    <li class="">
                        <a href="{{ URL::to('/admin/physician') }}" title="Add Physician"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Physician</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/physicianlist') }}" title="All Physician"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">All Physician</span></a>
                    </li>
                </ul>
            </li>


            <li class="top-menu-invisible">
                <a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Staff</span></a>
                <ul>
                    <li class="">
                        <a href="{{ URL::to('/admin/staff') }}" title="Add Staff"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Staff</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/stafflist') }}" title="All Staff"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">All Staff</span></a>
                    </li>
                </ul>
            </li>

            <li class="top-menu-invisible">
                <a href="#"><i class="fa fa-lg fa-fw fa-cogs"></i> <span class="menu-item-parent">Basic Setup</span></a>
                <ul>
                    <li class="">
                        <a href="{{ URL::to('/admin/module') }}" title="Add Modules"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Modules</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/modulelist') }}" title="All Modules"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">All Modules</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/modulewiserole') }}" title="Add Module Wise Role"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Module Wise Role</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/modulewiserolelist') }}" title="Manage Module Wise"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Module Wise</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/staffcategory') }}" title="Add Staff Category"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Staff Category</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/staffcategorylist') }}" title="Manage Staff Category"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Staff Category</span></a>
                    </li>
                    <!-- Question Section for Ptient -->
                    <li class="">
                        <a href="{{ URL::to('/admin/qsection') }}" title="Add Section"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Section</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/sectionlist') }}" title="Manage Section"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Section</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/question') }}" title="Add Question"><i class="fa fa-lg fa-fw fa-plus-square-o"></i> <span class="menu-item-parent">Add Question</span></a>
                    </li>
                    <li class="">
                        <a href="{{ URL::to('/admin/questionlist') }}" title="Manage Question"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Question</span></a>
                    </li>

                </ul>
            </li>



        </ul>
    </nav>
    

    <span class="minifyme" data-action="minifyMenu"> 
        <i class="fa fa-arrow-circle-left hit"></i> 
    </span>

</aside>


