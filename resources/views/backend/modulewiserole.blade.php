@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li>
            <li>Basic Setup</li>
            <li>Module Wise Role Add</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-edit fa-fw "></i> 
                            Module
                        <span>> 
                            Module Wise Role Add                                
                        </span>
                    </h1>
            </div>
        </div>


        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- START ROW -->

            <div class="row">

                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                        
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Add Module Wise Role form</h2>
                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <form action="" method="POST" class="smart-form">
                                    
                                    @csrf

                                    <div class="smart-form">
                                        
                                        @if(session()->has('success'))
                                            <header class="admin_success_msg">
                                                {{ session()->get('success') }}
                                            </header>
                                        @endif

                                        <fieldset>                                            

                                            <section>
                                                <label class="select">Module
                                                    <select name="module_id">
                                                        <option value="0" selected disabled>Module</option>
                                                        @if(count($modules)) 
                                                            @foreach ($modules as $module)
                                                            <option value="{{ $module->id }}">{{$module->module_name}}</option>
                                                            @endforeach 
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                </label>
                                                @if ($errors->has('module_id'))
                                                    <p class="text-danger">{{ $errors->first('module_id') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Role Name</label>
                                                <label class="input">
                                                    <input type="text" name="role_name" class="input-sm">
                                                </label>
                                                @if ($errors->has('role_name'))
                                                    <p class="text-danger">{{ $errors->first('role_name') }}</p>
                                                @endif
                                            </section>
                                        </fieldset>

                                        <footer>
                                            <button type="submit" @click="save" class="btn btn-primary">
                                                Submit
                                            </button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">
                                                Back
                                            </button>
                                        </footer>
                                    </div>

                                </form>                                

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- END COL -->



            </div>

            <!-- END ROW -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection