@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li><li>Physician</li><li>Physician Data Tables</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i> 
                        Physician
                    <span>> 
                        Physician Tables
                    </span>
                </h1>
            </div>
        </div>
        
        <!-- widget grid -->
        <section id="widget-grid" class="">
        
            <!-- row -->
            <div class="row">
        
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Physician Data Tables </h2>            
                        </header>


        
                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                @if(session()->has('success'))
                                    <header class="admin_success_msg">
                                        {{ session()->get('success') }}
                                    </header>
                                @endif 
        
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th data-hide="phone">SL</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Clinic Name</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Physician Name </th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Email </th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Phone </th>
                                            <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($physicians))
                                            @foreach($physicians as $key=>$physician)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $physician->clinic_name }}</td>                                               
                                                    <td>{{ $physician->name }}</td>                                                
                                                    <td>{{ $physician->email }}</td>                                                
                                                    <td>{{ $physician->phone }}</td>                                              
                                                    <td>
                                                        <a href="#physicianModal{{$key+1}}"  class="btn btn-primary" data-toggle="modal">Edit</a>
                                                        <a href="{{ URL::to('/admin/deletephysician/' . $physician->id) }}" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-danger">Delete</a>                                                        
                                                    </td>

                                                    <!-- Modal -->                                                
                                                    <div class="modal fade" id="physicianModal{{$key+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Update Physician {{ $physician->name }} Information</h5>                                                            
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form action="{{ URL::to('/admin/updatephysician/' . $physician->id) }}" method="post">
                                                                    <div class="modal-body">
                                                                    
                                                                        @csrf

                                                                        @if(session()->has('updatemsg'))
                                                                            <header class="admin_success_msg">
                                                                                {{ session()->get('updatemsg') }}
                                                                            </header>
                                                                        @endif

                                                                        <fieldset class="smart-form">
                                                                            <section>
                                                                                <label class="select">Clinic/Location
                                                                                <select name="location_id">
                                                                                    <option value="0" selected="" disabled="">Clinic/Location</option>
                                                                                    @if (count($locations))
                                                                                        @foreach($locations as $location)
                                                                                            <option value="{{ $location->id }}" {{ $physician->location_id == $location->id ? 'selected="selected"' : '' }}>
                                                                                                {{ $location->clinic_name }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </select>
                                                                                <i style="top: 31px !important;"></i> 
                                                                                </label>
                                                                                @if ($errors->has('location_id'))
                                                                                    <p class="text-danger">{{ $errors->first('location_id') }}</p>
                                                                                @endif
                                                                            </section>

                                                                            <section>
                                                                                <label class="label">Physician Name</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="name" value="{{ $physician->name }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('name'))
                                                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                                                @endif
                                                                            </section>
                                                                            <section>
                                                                                <label class="label">Email</label>
                                                                                <label class="input">
                                                                                    <input type="email" name="email" value="{{ $physician->email }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('email'))
                                                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                                                @endif
                                                                            </section>
                                                                            <section>
                                                                                <label class="label">Phone</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="phone" value="{{ $physician->phone }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('phone'))
                                                                                    <p class="text-danger">{{ $errors->first('phone') }}</p>
                                                                                @endif
                                                                            </section>
                                                                            <section>
                                                                                <label class="label">License</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="license" value="{{ $physician->license }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('license'))
                                                                                    <p class="text-danger">{{ $errors->first('license') }}</p>
                                                                                @endif
                                                                            </section>
                                                                        </fieldset>
                                                                        
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->
        
                        </div>
                        <!-- end widget div -->
        
                    </div>
                    <!-- end widget -->
        
                </article>
                <!-- WIDGET END -->
        
            </div>
        
            <!-- end row -->
        
        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection