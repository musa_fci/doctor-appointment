@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li><li>Basic Setup</li><li>Section Data Tables</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i> 
                        Basic Setup
                    <span>> 
                        Section Data Tables
                    </span>
                </h1>
            </div>
        </div>
        
        <!-- widget grid -->
        <section id="widget-grid" class="">
        
            <!-- row -->
            <div class="row">
        
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Section Data Tables </h2>            
                        </header>

                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                            
                                @if(session()->has('success'))
                                    <header class="admin_success_msg">
                                        {{ session()->get('success') }}
                                    </header>
                                @endif

                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th data-hide="phone">SL</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Section Title</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Section Sub-Title </th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Description </th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Is Required </th>
                                            <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($sections))
                                            @foreach($sections as $key=>$section)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $section->title }}</td>
                                                    <td>{{ $section->sub_title }}</td>                                                
                                                    <td>{{ $section->description }}</td>
                                                    @if($section->is_required == 0)
                                                        <td>No</td>
                                                    @else
                                                        <td>Yes</td>
                                                    @endif
                                                    <td>
                                                        <a href="#sectionModal{{ $key+1 }}"  class="btn btn-primary" data-toggle="modal">Edit</a>
                                                        <a href="{{ URL::to('/admin/deletesection/' . $section->id) }}" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-danger">Delete</a>
                                                    </td>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="sectionModal{{ $key+1 }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Update Section {{ $section->title }} Information</h5>                                                            
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>

                                                                <form action="{{ URL::to('/admin/updatesection/' . $section->id) }}" method="post">
                                                                    <div class="modal-body">
                                                                        
                                                                        @csrf

                                                                        @if(session()->has('updatemsg'))
                                                                            <header class="admin_success_msg">
                                                                                {{ session()->get('updatemsg') }}
                                                                            </header>
                                                                        @endif
                                                                        
                                                                        <fieldset class="smart-form">
                                                                            <section>
                                                                                <label class="label">Title</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="title" value="{{ $section->title }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('title'))
                                                                                    <p class="text-danger">{{ $errors->first('title') }}</p>
                                                                                @endif
                                                                            </section>
                                                                            <section>
                                                                                <label class="label">Sub-Title</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="sub_title" value="{{ $section->sub_title }}" class="input-sm">
                                                                                </label>                                                                
                                                                            </section>
                                                                            <section>
                                                                                <label class="label">Description</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="description" value="{{ $section->description }}" class="input-sm">
                                                                                </label>                                                                
                                                                            </section>                                                                                                                                
                                                                        </fieldset>
                                                                        
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                                    </div>
                                                                </form>

                                                            </div>                                                        
                                                        </div>
                                                    </div>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->
        
                        </div>
                        <!-- end widget div -->
        
                    </div>
                    <!-- end widget -->
        
                </article>
                <!-- WIDGET END -->
        
            </div>
        
            <!-- end row -->
        
        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection