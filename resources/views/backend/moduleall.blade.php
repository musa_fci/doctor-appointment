@extends('backend.app')

@section('content')

<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span> 
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li><li>Modules</li><li>Modules Data Tables</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i> 
                        Modules 
                    <span>> 
                        Modules Data Tables
                    </span>
                </h1>
            </div>
        </div>
        
        <!-- widget grid -->
        <section id="widget-grid" class="">
        
            <!-- row -->
            <div class="row">
        
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Modules Data Tables </h2>
                        </header>
        
                        <!-- widget div-->
                        <div>
        
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                @if(session()->has('success'))
                                    <header class="admin_success_msg">
                                        {{ session()->get('success') }}
                                    </header>
                                @endif
        
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>			                
                                        <tr>
                                            <th data-hide="phone">SL</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Module Name</th>
                                            <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($modules))
                                            @foreach($modules as $key=>$module)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $module->module_name }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#moduleModal{{$key+1}}">Edit</button>
                                                        <a href="{{ URL::to('/admin/deletemodule/' . $module->id) }}" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-danger">Delete</a>
                                                    </td>

                                                    <!-- Modal -->                                                
                                                    <div class="modal fade" id="moduleModal{{$key+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Update Module Name "{{ $module->module_name }}"</h5>                                                            
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form action="{{ URL::to('/admin/updatemodule/' . $module->id) }}" method="post">
                                                                    <div class="modal-body">
                                                                        @csrf
                                                                        @if(session()->has('updatemsg'))
                                                                            <header class="admin_success_msg">
                                                                                {{ session()->get('updatemsg') }}
                                                                            </header>
                                                                        @endif
                                                                        <fieldset class="smart-form">
                                                                            <section>
                                                                                <label class="label">Module Name</label>
                                                                                <label class="input">
                                                                                    <input type="text" name="module_name" value="{{ $module->module_name }}" class="input-sm">
                                                                                </label>
                                                                                @if ($errors->has('module_name'))
                                                                                    <p class="text-danger">{{ $errors->first('module_name') }}</p>
                                                                                @endif
                                                                            </section>
                                                                        </fieldset>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->
        
                        </div>
                        <!-- end widget div -->
        
                    </div>
                    <!-- end widget -->
        
                </article>
                <!-- WIDGET END -->
        
            </div>
        
            <!-- end row -->
        
        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->

@endsection