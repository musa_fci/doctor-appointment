@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li>
            <li>Admin</li>
            <li>Admin Profile</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-edit fa-fw "></i> 
                            Locations 
                        <span>> 
                            Add Location                                
                        </span>
                    </h1>
            </div>
        </div>


        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- START ROW -->

            <div class="row">

                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Add location form</h2>
                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                
                                <form action="" method="POST" class="smart-form">
                                
                                    @csrf

                                    <div class="smart-form">

                                        @if(session()->has('success'))
                                            <header class="admin_success_msg">
                                                {{ session()->get('success') }}
                                            </header>
                                        @endif 
                                       
                                        <input type="hidden" name="admin_id" value="2" class="input-sm">

                                        <fieldset>

                                            <section>
                                                <label class="label">Clinic Name</label>
                                                <label class="input">
                                                    <input type="text" name="clinic_name" value="{{ old('clinic_name') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('clinic_name'))
                                                    <p class="text-danger">{{ $errors->first('clinic_name') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Email</label>
                                                <label class="input">
                                                    <input type="email" name="email" value="{{ old('email') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('email'))
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </section>
                                            
                                            <section>
                                                <label class="label">Password</label>
                                                <label class="input">
                                                    <input type="password" name="password" class="input-sm">
                                                </label>
                                                @if ($errors->has('password'))
                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Phone</label>
                                                <label class="input">
                                                    <input type="text" name="phone" value="{{ old('phone') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('phone'))
                                                    <p class="text-danger">{{ $errors->first('phone') }}</p>
                                                @endif
                                            </section>

                                            <section>
                                                <label class="label">Website</label>
                                                <label class="input">
                                                    <input type="text" name="website" value="{{ old('website') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('website'))
                                                    <p class="text-danger">{{ $errors->first('website') }}</p>
                                                @endif                                                
                                            </section>


                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="select">Country
                                                        <select name="country" id="country">
                                                            <option value="0" selected disabled>Select Country</option>
                                                            @if(count($countries)) 
                                                                @foreach ($countries as $country)
                                                                <option value="{{ $country->id }}">{{$country->country_name}}</option>
                                                                @endforeach 
                                                            @endif
                                                        </select>
                                                        <i style="top:31px !important;"></i>
                                                    </label>
                                                    @if ($errors->has('country'))
                                                        <p class="text-danger">{{ $errors->first('country') }}</p>
                                                    @endif
                                                </section>
                                                <section class="col col-4">
                                                    <label class="select">State
                                                        <select name="state" id="state">
                                                            <option value="0" selected disabled>State</option>
                                                        </select>
                                                        <i style="top:31px !important;"></i>
                                                    </label>
                                                    @if ($errors->has('state'))
                                                        <p class="text-danger">{{ $errors->first('state') }}</p>
                                                    @endif
                                                </section>
                                                <section class="col col-4">
                                                    <label class="select">City
                                                        <select name="city" id="city">
                                                            <option value="0" selected disabled>City</option>
                                                        </select>
                                                        <i style="top:31px !important;"></i>
                                                    </label>
                                                    @if ($errors->has('city'))
                                                        <p class="text-danger">{{ $errors->first('city') }}</p>
                                                    @endif
                                                </section>
                                            </div>


                                            <section>
                                                <label class="label">Address</label> 
                                                <label class="textarea">
                                                    <textarea name="address" rows="3" class="custom-scroll">{{ old('address') }}</textarea>
                                                </label> 
                                                @if ($errors->has('address'))
                                                    <p class="text-danger">{{ $errors->first('address') }}</p>
                                                @endif
                                            </section>


                                            <section>
                                                <label class="label">Post Code</label>
                                                <label class="input">
                                                    <input type="text" name="post_code" value="{{ old('post_code') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('post_code'))
                                                    <p class="text-danger">{{ $errors->first('post_code') }}</p>
                                                @endif
                                            </section>


                                            <section>
                                                <label class="label">EIN</label>
                                                <label class="input">
                                                    <input type="text" name="controlled_license" value="{{ old('controlled_license') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('controlled_license'))
                                                    <p class="text-danger">{{ $errors->first('controlled_license') }}</p>
                                                @endif
                                            </section>

                                            
                                            <section>
                                                <label class="label">Business Lic</label>
                                                <label class="input">
                                                    <input type="text" name="reg_no" value="{{ old('reg_no') }}" class="input-sm">
                                                </label>
                                                @if ($errors->has('reg_no'))
                                                    <p class="text-danger">{{ $errors->first('reg_no') }}</p>
                                                @endif
                                            </section>


                                            <section>
                                                <label class="label">Profile</label> 
                                                <label class="textarea">
                                                    <textarea name="profile" rows="3" class="custom-scroll">{{ old('profile') }}</textarea>
                                                </label> 
                                                <div class="note"><strong>Note:</strong> height of the textarea depends on the rows attribute.
                                                </div>
                                                @if ($errors->has('profile'))
                                                    <p class="text-danger">{{ $errors->first('profile') }}</p>
                                                @endif
                                            </section>

                                        </fieldset>

                                        <footer>
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                            <button type="button" class="btn btn-default" onclick="window.history.back();">
                                                Back
                                            </button>
                                        </footer>
                                    </div>
                                </form>
                                

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- END COL -->



            </div>

            <!-- END ROW -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
@endsection


@section('script')

//For Country Wise State

<script>
    $('#country').on('change',function(e){
        var country_id = e.target.value;
        $.ajax({
            url: "{{url('/getCountryWiseState')}}/"+country_id,
            type: "GET",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                var state =  $('#state').empty();
                state.append("<option value='0' selected disabled>State</option>");
                $.each(data.data,function(key,val){
                    state.append('<option value ="'+val.id+'">'+val.state_name+'</option>');
                });
            }
        })
    });

    //For State Wise City
    $('#state').on('change',function(e){
        var city_id = e.target.value;

        $.ajax({
            url: "{{url('/getcityWiseState')}}/"+city_id,
            type: "GET",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                var state =  $('#city').empty();
                state.append("<option value='0' selected disabled>City</option>");
                $.each(data.data,function(key,val){
                    state.append('<option value ="'+val.id+'">'+val.city_name+'</option>');
                });
            }
        })
    });
</script>
@endsection