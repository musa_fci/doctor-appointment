@extends('backend.app')

@section('content')
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <span class="ribbon-button-alignment"> 
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Dashboard</li>
            <li>Basic Setup</li>
            <li>Add Question</li>
        </ol>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-edit fa-fw "></i> 
                            Basic Setup 
                        <span>> 
                            Add Question                                
                        </span>
                    </h1>                        
            </div>
        </div>


        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- START ROW -->

            <div class="row">

                <!-- NEW COL START -->
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Add Question form</h2>
                        </header>

                        <!-- widget div-->
                            <div>

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <form action="" method="POST" class="smart-form">
                                        
                                        @csrf
                                        
                                        <div class="smart-form">

                                            @if(session()->has('success'))
                                                <header class="admin_success_msg">
                                                    {{ session()->get('success') }}
                                                </header>
                                            @endif

                                            <fieldset>
                                                <section>
                                                    <label class="select">Section
                                                    <select name="section_id">
                                                        <option value="0" selected="" disabled="">Section</option>
                                                        @if(count($sections)) 
                                                            @foreach ($sections as $section)
                                                            <option value="{{ $section->id }}">{{$section->title}}</option>
                                                            @endforeach 
                                                        @endif
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                    </label>
                                                    @if ($errors->has('section_id'))
                                                        <p class="text-danger">{{ $errors->first('section_id') }}</p>
                                                    @endif
                                                </section>
                                                
                                               

                                                <section>
                                                    <label class="label">Question Title</label>
                                                    <label class="input">
                                                    <input type="text" name="title" class="input-sm">
                                                    </label>                                            
                                                </section>

                                                <section>
                                                    <label class="label">Question Sub-Title</label>
                                                    <label class="input">
                                                        <input type="text" name="sub_title" class="input-sm">
                                                    </label>                                            
                                                </section>

                                                <section>
                                                    <label class="label">Question Description</label>
                                                    <label class="input">
                                                        <input type="text" name="description" class="input-sm">
                                                    </label>                                            
                                                </section>

                                                <section>
                                                    <label class="label">Question</label>
                                                    <label class="input">
                                                        <input type="text" name="question" class="input-sm">
                                                    </label>
                                                    @if ($errors->has('question'))
                                                        <p class="text-danger">{{ $errors->first('question') }}</p>
                                                    @endif
                                                </section>
                                                
                                                <section>
                                                    <label class="select">Question Type
                                                    <select name="question_type" id="questionType">
                                                        <option value="0" selected="" disabled="">Question Type</option>                                                    
                                                        <option value="1">Checkbox</option>                                                    
                                                        <option value="2">Input Field</option>                                                    
                                                        <option value="3">Radio Button</option>                                                    
                                                    </select>
                                                    <i style="top:31px !important;"></i>
                                                    </label>
                                                    @if ($errors->has('question_type'))
                                                        <p class="text-danger">{{ $errors->first('question_type') }}</p>
                                                    @endif
                                                </section>

                                                <section id="checkboxOption">
                                                    <label class="label">Add Checkbox Option</label>
                                                    
                                                    <div class="input-group control-group after-add-more">
                                                        <label class="input">
                                                            <input type="text" name="option[]" class="input-sm" placeholder="Enter Name Here">
                                                        </label>                                                            
                                                            <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>                                                            
                                                    </div>

                                                    <!-- Copy Fields -->
                                                    <div class="copy hide">
                                                        <div class="control-group input-group" style="margin-top:10px">
                                                            <label class="input">
                                                                <input type="text" name="option[]" class="input-sm" placeholder="Enter Name Here">
                                                            </label>                                                           
                                                                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i></button>                                                                
                                                        </div>
                                                    </div>
                                                </section>

                                                <section id="radioBtnOption">
                                                    <label class="label">Add Radio Button Option</label>                                                    

                                                        <div class="input-group control-group after-add-more">
                                                            <label class="input">
                                                                <input type="text" name="option[]" class="input-sm" placeholder="Enter Name Here">
                                                            </label>                                                            
                                                                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>                                                            
                                                        </div>

                                                        <!-- Copy Fields -->
                                                        <div class="copy hide">
                                                            <div class="control-group input-group" style="margin-top:10px">
                                                                <label class="input">
                                                                    <input type="text" name="option[]" class="input-sm" placeholder="Enter Name Here">
                                                                </label>                                                           
                                                                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i></button>                                                                
                                                            </div>
                                                        </div>                                                
                                                </section>

                                            </fieldset>

                                            <footer>
                                                <button type="submit" class="btn btn-primary">
                                                    Submit
                                                </button>
                                                <button type="submit" class="btn btn-default" onclick="window.history.back();">
                                                    Back
                                                </button>
                                            </footer>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- END COL -->
            </div>

            <!-- END ROW -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

@endsection


@section('script')

    <script>
        $(function() {
            $('#checkboxOption').hide();
            $('#radioBtnOption').hide();

            $('#questionType').change(function(){
                if($('#questionType').val() == 1) {
                    $('#radioBtnOption').hide();
                    $('#checkboxOption').show();                    
                }else if($('#questionType').val() == 3) {
                    $('#checkboxOption').hide();
                    $('#radioBtnOption').show();
                } else {
                    $('#checkboxOption').hide();
                    $('#radioBtnOption').hide(); 
                } 
            });
        });    
    </script>

    <script type="text/javascript">

        $(document).ready(function() {
            $(".add-more").click(function(){ 
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });

            $("body").on("click",".remove",function(){ 
                $(this).parents(".control-group").remove();
            });
        });

    </script>

@endsection