<!doctype html>
<html lang="en">

    @include('frontend._partial._head')

    <body  id="ic-first-header">

        @include('frontend._partial._header')
        @include('frontend._partial._breadcumb')

        <section id="ic-apply-form" class="ic-profile-edit">

            @section('content')
            @show

        </section>

        @include('frontend._partial._footer')

        @include('frontend._partial._script')
    
    </body>
</html>
