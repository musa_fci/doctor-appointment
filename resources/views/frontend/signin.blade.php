<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MedCardNow</title>

    <!-- ===== Stylesheet Files Link ===== -->

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">
</head>

<body>

    <!-- ===== Header Part HTML Start ===== -->

    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('frontend/images/footer-logo.png') }}" alt="Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fas fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mr-lg-4">
                        <a class="nav-link" href="index.html"><i class="fas fa-chevron-left"></i> Back To Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('frontend/images/nav-logo1.png') }}" alt="Hippo">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('frontend/images/nav-logo2.png') }}" alt="Secure">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- ===== Header Part HTML End ===== -->

    <!-- ===== Application Form Part HTML Start ===== -->

    <section id="ic-apply-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ic-apply-txt text-center">
                        <h5>Online Medical Evaluation - <span class="ic-green">Money Back Guarantee</span> - <span
                                class="ic-sky">Instant Rec Download</span></h5>
                        <div class="ic-apply-notice">
                            <h6>Welcome to Med Card Now California’s Online Medical Marijuana Certification System</h6>
                        </div>
                    </div>
                    <div class="ic-apply-field ic-sign-in">
                        <div class="row">
                            <div class="col-lg-6 col-md-10 m-auto">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                            role="tab" aria-controls="home" aria-selected="true">Admin <span class="d-none d-sm-inline d-md-inline d-lg-inline">Login</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile"
                                            role="tab" aria-controls="profile" aria-selected="false">Clinic / Physician
                                            <span class="d-none d-sm-inline d-md-inline d-lg-inline">Login</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact"
                                            role="tab" aria-controls="contact" aria-selected="false">Patient <span class="d-none d-sm-inline d-md-inline d-lg-inline">Login</span></a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel"
                                        aria-labelledby="home-tab">
                                        <h6>Admin Login</h6>
                                        <form>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Email" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Password" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-6">
                                                            <label class="ic-checkbox">
                                                                <input type="checkbox"> <span class="checktext">Remember
                                                                    me</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6 col-6 text-right">
                                                            <a href="#" class="forgot">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 text-center">
                                                    <button type="submit" class="ic-sign-btn">Login To My
                                                        Account</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="profile" role="tabpanel"
                                        aria-labelledby="profile-tab">
                                        <h6>Clinic / Physician Login</h6>
                                        <form>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Email" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Password" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-6">
                                                            <label class="ic-checkbox">
                                                                <input type="checkbox"> <span class="checktext">Remember
                                                                    me</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6 col-6 text-right">
                                                            <a href="#" class="forgot">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 text-center">
                                                    <button type="submit" class="ic-sign-btn">Login To My
                                                        Account</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="contact" role="tabpanel"
                                        aria-labelledby="contact-tab">
                                        <h6>Pateint Login</h6>
                                        <form>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Email" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <input type="text" placeholder="Password" class="form-control">
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-6">
                                                            <label class="ic-checkbox">
                                                                <input type="checkbox"> <span class="checktext">Remember
                                                                    me</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6 col-6 text-right">
                                                            <a href="#" class="forgot">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 text-center">
                                                    <button type="submit" class="ic-sign-btn">Login To My
                                                        Account</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ===== Application Form Part HTML End ===== -->

    <!-- ===== Javascript Files Source Path ===== -->

    <script src="{{ asset('frontend/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/slick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.en.js') }}"></script>
    <script src="{{ asset('frontend/js/custom.js') }}"></script>
</body>

</html>