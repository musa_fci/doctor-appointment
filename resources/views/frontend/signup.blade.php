@extends('frontend.app') 

@section('content')
<div class="container">

    <div class="row ic-apply-field ic-sign-in" style="background: rgb(246, 246, 246);margin-bottom: 40px;padding: 0px 0px 13px 0px;">
        <div class="col-lg-6 ic-profile-round-radio mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="ic-yes-no">
                        <li>
                            <label class="ic-checkbox">
                                <input type="radio" name="contact" id="newPatient" checked>
                                <span class="checktext">New Patient</span>
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        <li>
                            <label class="ic-checkbox">
                                <input type="radio" name="contact" id="existingPatient">
                                <span class="checktext">Existing Patient</span>
                                <span class="checkmark"></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="row" id="forNew" style="display:block">
        <div class="col-lg-12">
            <div class="ic-apply-field ic-sign-in">
                <div class="row">
                    <div class="col-lg-12 col-md-12 m-auto">

                        @if(session()->has('success'))
                        <p class="alert alert-success text-center">
                            {{ session()->get('success') }}
                        </p>
                        @endif

                        <form method="post" action="" id="login_success" enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="step1-tab" data-toggle="tab" href="#step1" role="tab" aria-controls="step1" aria-selected="true">Step 01</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="step2-tab" data-toggle="tab" href="#step2" role="tab" aria-controls="step2" aria-selected="false">Step 02</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="step3-tab" data-toggle="tab" href="#step3" role="tab" aria-controls="step3" aria-selected="false">Step 03</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="step6-tab" data-toggle="tab" href="#step6" role="tab" aria-controls="step6" aria-selected="false">Step 04</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab">
                                    <div class="col-lg-12">
                                        <h2><i class="fas fa-plus-circle"></i> General Information</h2>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Location</label>
                                                <select id="location" name="location_id">
                                                    <option value="0" selected disabled>Select Location</option>
                                                    @if(count($locations)) 
                                                        @foreach ($locations as $location)
                                                        <option value="{{ $location->id }}">{{$location->clinic_name}}</option>
                                                        @endforeach 
                                                    @endif
                                                </select>
                                                @if ($errors->has('location_id'))
                                                <p class="text-danger">{{ $errors->first('location_id') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Physician</label>
                                                <select id="physician" name="physician_id">
                                                    <option value="0" selected disabled>Select Physician</option>

                                                </select>
                                                @if ($errors->has('physician_id'))
                                                <p class="text-danger">{{ $errors->first('physician_id') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>First Name <span class="ic-required">*</span></label>
                                                <input type="text" name="f_name" value="{{ old('f_name') }}" placeholder="Enter First Name" class="form-control"> 
                                                @if ($errors->has('f_name'))
                                                    <p class="text-danger">{{ $errors->first('f_name') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Last Name <span class="ic-required">*</span></label>
                                                <input type="text" name="l_name" value="{{ old('l_name') }}" placeholder="Enter Last Name" class="form-control"> 
                                                @if ($errors->has('l_name'))
                                                    <p class="text-danger">{{ $errors->first('l_name') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Email</label>
                                                <input type="email" name="email" value="{{ old('email') }}" placeholder="Enter Email Address" class="form-control"> 
                                                @if ($errors->has('email'))
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-2 col-sm-2 col-sm-2 col-md-2">
                                                <label>Country Code</label>
                                                <select name="country_code" id="country_code">
                                                    <option value="0" selected disabled>Select Code</option>
                                                    @if(count($countries)) 
                                                        @foreach ($countries as $country)
                                                        <option value="{{ $country->phonecode }}">{{$country->country_name}} - {{ $country->phonecode }}</option>
                                                        @endforeach 
                                                    @endif
                                                </select>
                                                @if ($errors->has('country_code'))
                                                <p class="text-danger">{{ $errors->first('country_code') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-4 col-sm-4 col-md-4">
                                                <label>Phone Number</label>
                                                <input type="text" id="phone_number" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number" class="form-control"> 
                                                @if ($errors->has('phone_number'))
                                                    <p class="text-danger">{{ $errors->first('phone_number') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Password <span class="ic-required">*</span></label>
                                                <input type="password" name="password" placeholder="xxxxxxxx" class="form-control"> 
                                                @if ($errors->has('password'))
                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Confirm Password <span class="ic-required">*</span></label>
                                                <input type="password" name="password_confirmation" placeholder="xxxxxxxx" class="form-control">
                                            </div>

                                            <div class="col-lg-12 col-sm-12 col-md-12">
                                                <label>Address</label>
                                                <input type="text" name="address" value="{{ old('address') }}" placeholder="Enter Address" class="form-control"> 
                                                @if ($errors->has('address'))
                                                    <p class="text-danger">{{ $errors->first('address') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Country</label>
                                                <select name="country" id="country">
                                                    <option value="0" selected disabled>Select Country</option>
                                                    @if(count($countries)) 
                                                        @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}">{{$country->country_name}}</option>
                                                        @endforeach 
                                                    @endif
                                                </select>
                                                @if ($errors->has('country'))
                                                    <p class="text-danger">{{ $errors->first('country') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                                        <label>State</label>
                                                        <select name="state" id="state">
                                                            <option value="0" selected disabled>State</option>

                                                        </select>
                                                        @if ($errors->has('state'))
                                                            <p class="text-danger">{{ $errors->first('state') }}</p>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                                        <label>City</label>
                                                        <select name="city" id="city">
                                                            <option value="0" selected disabled>City</option>
                                                        </select>
                                                        @if ($errors->has('city'))
                                                            <p class="text-danger">{{ $errors->first('city') }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>State ID</label>
                                                <input type="text" name="state_id" value="{{ old('state_id') }}" placeholder="Enter ID" class="form-control"> 
                                                @if ($errors->has('state_id'))
                                                    <p class="text-danger">{{ $errors->first('state_id') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>ZIP Code</label>
                                                <input type="text" name="zip_code" value="{{ old('zip_code') }}" placeholder="09876321" class="form-control"> 
                                                @if ($errors->has('zip_code'))
                                                    <p class="text-danger">{{ $errors->first('zip_code') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Date of Birth <span class="ic-required">*</span></label>
                                                <input type="text" name="dob" value="{{ old('dob') }}" placeholder="DD/MM/YYYY" class="form-control datepicker-here" data-language='en'> 
                                                @if ($errors->has('dob'))
                                                    <p class="text-danger">{{ $errors->first('dob') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Gender</label>
                                                <select name="gender">
                                                    <option value="0" selected disabled>Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                                @if ($errors->has('gender'))
                                                    <p class="text-danger">{{ $errors->first('gender') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Initial Visit <span class="ic-required">*</span></label>
                                                <input type="text" name="initial_visit" value="{{ old('initial_visit') }}" placeholder="08/22/2019" class="form-control datepicker-here" data-language='en'> 
                                                @if ($errors->has('initial_visit'))
                                                    <p class="text-danger">{{ $errors->first('initial_visit') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Expiration</label>
                                                <input type="text" name="expiration" value="{{ old('expiration') }}" placeholder="DD/MM/YYYY" class="form-control datepicker-here" data-language='en'> 
                                                @if ($errors->has('expiration'))
                                                    <p class="text-danger">{{ $errors->first('expiration') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Caregiver</label>
                                                <input type="text" name="caregiver" value="{{ old('caregiver') }}" placeholder="Enter Here" class="form-control"> 
                                                @if ($errors->has('caregiver'))
                                                    <p class="text-danger">{{ $errors->first('caregiver') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Internal #</label>
                                                <input type="text" name="internal" value="{{ old('internal') }}" placeholder="Enter Here" class="form-control"> 
                                                @if ($errors->has('internal'))
                                                    <p class="text-danger">{{ $errors->first('internal') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Social Security Number</label>
                                                <input type="password" name="social_security_no" value="{{ old('social_security_no') }}" placeholder="xxxxxxxx" class="form-control"> 
                                                @if ($errors->has('social_security_no'))
                                                    <p class="text-danger">{{ $errors->first('social_security_no') }}</p>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Rec Type</label>
                                                <select name="rec_type">
                                                    <option value="0" selected disabled>Select Recommendation Type</option>
                                                    <option value="Recommendation One">Recommendation One</option>
                                                    <option value="Recommendation Two">Recommendation Two</option>
                                                </select>
                                                @if ($errors->has('rec_type'))
                                                    <p class="text-danger">{{ $errors->first('rec_type') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Parent / Guardian</label>
                                                <select name="guardian">
                                                    <option value="0" selected disabled>Select Guardian</option>
                                                    <option value="Father">Father</option>
                                                    <option value="Mother">Mother</option>
                                                    <option value="Brother">Brother</option>
                                                    <option value="Sister">Sister</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                                @if ($errors->has('guardian'))
                                                    <p class="text-danger">{{ $errors->first('guardian') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 ic-profile-radio mb-3 mb-lg-0">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>Contact Options</label>
                                                    </div>
                                                    <div class="col-lg-6 col-6">
                                                        <label class="ic-checkbox">
                                                            <input type="radio" name="contact_option" value="Email">
                                                            <span class="checktext">Email</span>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-6 col-6">
                                                        <label class="ic-checkbox">
                                                            <input type="radio" name="contact_option" value="SMS">
                                                            <span class="checktext">SMS</span>
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                @if ($errors->has('contact_option'))
                                                    <p class="text-danger">{{ $errors->first('contact_option') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Referred By</label>
                                                <select name="referred_by">
                                                    <option value="0" selected disabled>Select</option>
                                                    <option value="Google">Google</option>
                                                    <option value="Facebook">Facebook</option>
                                                    <option value="Yahoo">Yahoo</option>
                                                    <option value="Twitter">Twitter</option>
                                                </select>
                                                @if ($errors->has('referred_by'))
                                                    <p class="text-danger">{{ $errors->first('referred_by') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>MMU ID #</label>
                                                <input type="text" name="mmu_id" value="{{ old('mmu_id') }}" placeholder="MMU ID #" class="form-control"> 
                                                @if ($errors->has('mmu_id'))
                                                    <p class="text-danger">{{ $errors->first('mmu_id') }}</p>
                                                @endif
                                            </div>

                                            <!-- <div class="col-lg-12 text-right">
                                                <button type="button" class="mr-3">Previous</button>
                                                <button type="button">Next</button>
                                            </div> -->


                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="step2" role="tabpanel" aria-labelledby="step2-tab">
                                    <div class="row">
                                        @if(count($sections))
                                            @foreach($sections as $key=>$section)
                                                <div class="col-lg-12">

                                                    <h2><i class="fas fa-plus-circle"></i>{{ $section->title }}</h2>
                                                    <h5>{{ $section->sub_title }}</h5>
                                                    <p>{{ $section->description }}</p>
                                                    <!-- <p>{{ $section }}</p> -->
                                             

                                                    <!--Loop for checkbox-->
                                                    @foreach($section->questions as $question)

                                                        @if($question->question_type == 1)

                                                        <input type="hidden" name="symtomps_question_id[{{ $question->id }}]" value="{{ $question->id }}">

                                                        <p>{{ $question->question }}</p>
                                                        <div class="row">
                                                            @foreach($question->options as $option)
                                                                <div class="col-6 col-sm-6 col-md-4 col-lg-3 ic-profile-checkbox">                                                                
                                                                    <label class="ic-checkbox">
                                                                        
                                                                        <input type="checkbox" value="{{ $option->id }}" name="medical_symtomps[{{ $option->id }}]">
                                                                        <span class="checktext">{{ $option->option }}</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            @endforeach

                                                            @if ($errors->has('medical_symtomps'))
                                                                <p class="text-danger">{{ $errors->first('medical_symtomps') }}</p>
                                                            @endif
                                                        </div>
                                                        @endif
                                                    @endforeach


                                                    <!--Loop for input box-->
                                                    @foreach($section->questions as $question)
                                                        @if($question->question_type == 2)
                                                            <div class="row">                                                            
                                                                @foreach($question->options as $option)
                                                                    <div class="col-6 col-lg-6 col-md-6">
                                                                        <label>{{ $question->question }}</label>
                                                                        <input type="hidden" name="significant_question_id[{{ $question->id }}]" value="{{ $question->id }}">
                                                                        <input type="text" name="significant_issue[{{ $option->id }}]" placeholder="Enter Text Here" class="form-control">
                                                                        @if ($errors->has('significant_issue'))
                                                                            <p class="text-danger">{{ $errors->first('significant_issue') }}</p>
                                                                        @endif
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                    

                                                    <!--Loop for radio button-->
                                                    @foreach($section->questions as $question)
                                                        @if($question->question_type == 3)
                                                            <div class="row">
                                                                @foreach($question->options as $option)
                                                                    <div class="col-lg-12">
                                                                        <!-- <h1>{{ $option->option }}</h1> -->
                                                                        <div class="row">
                                                                            <div class="col-sm-6 col-md-4 col-lg-4 ic-profile-round-radio">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>{{ $question->question }}</label>
                                                                                        <input type="hidden" name="medical_info_question_id[{{ $question->id }}]" value="{{ $question->id }}">
                                                                                    </div>
                                                                                    <div class="col-lg-12">
                                                                                        <ul class="ic-yes-no">
                                                                                            <li>
                                                                                                <label class="ic-checkbox">
                                                                                                    <input type="radio" value="1" name="medical_info[{{$question->id}}]">
                                                                                                    <span class="checktext">YES</span>
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </li>
                                                                                            <li>
                                                                                                <label class="ic-checkbox">
                                                                                                    <input type="radio" value="0" checked name="medical_info[{{$question->id}}]"> 
                                                                                                    <span class="checktext">No</span>
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    @if ($errors->has('medical_info'))
                                                                                        <p class="text-danger">{{ $errors->first('medical_info') }}</p>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                </div>
                                            @endforeach
                                        @endif

                                        <!-- <div class="col-lg-12 text-right">
                                            <button type="button" class="mr-3">Previous</button>
                                            <button type="button">Next</button>
                                        </div> -->

                                    </div>
                                </div>

                                <div class="tab-pane fade" id="step3" role="tabpanel" aria-labelledby="step3-tab">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h2><i class="fas fa-plus-circle"></i> Drivers License / State ID And
                                                    Photo
                                                    <span>(Optional)</span></h2>

                                            <div class="row">
                                                <div class="col-lg-12 text-center">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-lg-3">
                                                            <div class="ic-profile-upload">
                                                                <span>Drivers Lic / State ID Front</span>
                                                                <ul>
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Camera
                                                                            <input type="button" onclick="callCamera()" style="display:none">
                                                                        </label>
                                                                    </li>                                    
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Capture
                                                                            <input type="button" onClick="takeSnapshotForStateIdFront()" style="display:none">
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                                <input type="hidden" value="" name="state_id_front" id="state_id_front">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-6 col-lg-3">
                                                            <div class="ic-profile-upload">
                                                                <span>Drivers Lic / State ID Back</span>
                                                                <ul>
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Camera
                                                                            <input type="button" onclick="callCamera()" style="display:none">
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Capture
                                                                            <input type="button" onClick="takeSnapshotForStateIdBack()" style="display:none">
                                                                        </label>
                                                                    </li>                                                                    
                                                                </ul>
                                                                <input type="hidden" value="" name="state_id_back" id="state_id_back">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-lg-3">
                                                            <div class="ic-profile-upload">
                                                                <span>Patient Photo <br> Required for ID
                                                                        Card</span>
                                                                <ul>
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Camera
                                                                            <input type="button" onclick="callCamera()" style="display:none">
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label class="file-type">
                                                                            Capture
                                                                            <input type="button" onClick="takeSnapshotForPatient()" style="display:none">
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                                <input type="hidden" value="" name="patient_photo_id" id="patient_photo_id">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6 col-lg-3" id="web_cam_show"></div>
                                                    </div>

                                                    <div id="webcam_snapshot"></div>

                                                </div>
                                                <div class="col-lg-12 mt-4 mt-lg-5">
                                                    <h2><i class="fas fa-plus-circle"></i> Patient Files
                                                            <span>(Optional)</span>
                                                        </h2>
                                                    <div class="ic-patient-files">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 text-center">
                                                                <div class="ic-ptf-item">
                                                                    <img src="{{ asset('frontend/images/upload-icon.png') }}" alt="Upload">
                                                                    <h6>Upload files from here</h6>
                                                                    <ul>
                                                                        <li>
                                                                            <label class="file-type">
                                                                                Select Files
                                                                                <input type="file" id="patient_file" name="patient_file[]" multiple>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 mb-5 mb-md-0 mb-lg-0">
                                                                <div class="ic-patient-form">
                                                                    <ul class="ic-patient-head">
                                                                        <li>
                                                                            File Name
                                                                        </li>                                                                        
                                                                    </ul>
                                                                    <div class="ic-patient-body">
                                                                        <h6 id="noFile">No patient files uploaded yet!</h6>                                                                        
                                                                        <ul class="list-group" id="showPatientFiles">
                                                                            <!-- After upload file..file will be show here -->
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="col-lg-12 text-right">
                                                    <button type="button" class="mr-3">Previous</button>
                                                    <button type="button">Next</button>
                                                </div> -->

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="step6" role="tabpanel" aria-labelledby="step6-tab">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h2><i class="fas fa-plus-circle"></i> Notes
                                                    <span>(Optional)</span></h2>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-9">
                                                    <label>Additional patient notes</label>
                                                    <textarea placeholder="Write a note here" name="patient_note" class="form-control"></textarea>
                                                </div>
                                                <div class="col-lg-12 mt-4 mt-lg-5">
                                                    <h2><i class="fas fa-plus-circle"></i> Physician / Stuff
                                                            Signature
                                                            <span>(Optional)</span></h2>
                                                </div>
                                                <div class="col-lg-12 col-md-9">
                                                    <div class="ic-sig-notice">
                                                        <p>By electronically signing this document, you declare under penalty of perjury that the information that you entered for this form is true and correct based on the patients answers. I also understand that an electronic signature has the same legal effect and can be enforced in the same way as a written signature.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-9">
                                                    <label>Signees Name</label>
                                                    <input type="text" name="signature_name" placeholder="Enter Text Here" class="form-control">
                                                </div>
                                                <div class="offset-lg-7"></div>
                                                <div class="col-lg-12 col-md-9">
                                                    <div class="ic-signature">
                                                        <h6>Please sign below using a stylus, your mouse, or
                                                                your finger and
                                                                then
                                                                click “SAVE”</h6>
                                                        <div class="ic-signature-inner">
                                                            <div class="ic-signature-canvas d-none d-sm-block d-md-block d-lg-block">
                                                                <canvas id="signature" width="450" height="150"></canvas>
                                                                <input type="hidden" value="" name="signature_data" id="signature_data">
                                                            </div>
                                                            <p>Save your signature before submit the form</p>
                                                            <button type="button" id="clear-signature" class="small-btn mr-2">Clear</button>
                                                            <button type="button" class="small-btn" onclick="getSignature()">Save</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 text-right">
                                                    <!-- <button type="button" class="mr-3">Edit</button> -->
                                                    <button type="submit">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        
                        <!-- <SECTION> -->
                            <!-- <DIV id="dropzone">
                                <FORM class="dropzone needsclick" id="demo-upload" action="/upload">
                                    <DIV class="dz-message needsclick">    
                                        Drop files here or click to upload.<BR>
                                        <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                                        files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                                    </DIV>
                                </FORM>
                            </DIV> -->
                        <!-- </SECTION> -->


                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row" id="forExisting" style="display:none;">
        <div class="col-lg-12">
            <div class="ic-apply-field ic-sign-in">
                <div class="row">
                    <div class="col-lg-12 col-md-12 m-auto">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab">
                                <div class="col-lg-12">
                                    <h2><i class="fas fa-plus-circle"></i> Existing Patient Login</h2>

                                    @if(session()->has('loginFail'))
                                        <p class="alert alert-danger" style="text-align: center;">
                                            {{ session()->get('loginFail') }}
                                        </p>
                                    @endif

                                    <form action="/patientLogin" method="post">
                                        <div class="row">
                                        
                                            @csrf		

                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Email <span class="ic-required"> * </span></label>
                                                <input type="email" name="email" placeholder="Enter Email Address" class="form-control">
                                                @if ($errors->has('email'))
                                                    <p style="color:#77021d">{{ $errors->first('email') }}</p>
                                                @endif
                                            </div>
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <label>Password <span class="ic-required"> * </span></label>
                                                <input type="password" name="password" placeholder="xxxxxxxx" class="form-control">
                                                @if ($errors->has('password'))
                                                    <p style="color:#77021d">{{ $errors->first('password') }}</p>
                                                @endif	
                                            </div>

                                            <div class="col-lg-12 text-right">
                                                <button type="submit" id="patientLogin">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection



@section('script')


<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    async function callCamera() {
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach( '#web_cam_show' );
    }
</script>
    



<!-- Code to handle taking the snapshot and displaying it locally -->
<script language="JavaScript">
    function takeSnapshotForPatient() {
        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('webcam_snapshot').innerHTML = 
                '<h2>Here is your image:</h2>' + 
                '<img src="'+data_uri+'"/>';
        } );

        var photo_data = $('#webcam_snapshot img').attr('src');
        $('#patient_photo_id').val(photo_data);
        console.log(photo_data);
    }


    function takeSnapshotForStateIdFront() {
        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('webcam_snapshot').innerHTML = 
                '<h2>Here is your image:</h2>' + 
                '<img src="'+data_uri+'"/>';
        } );

        var photo_data = $('#webcam_snapshot img').attr('src');
        $('#state_id_front').val(photo_data);
        console.log(photo_data);
    }


    function takeSnapshotForStateIdBack() {
        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('webcam_snapshot').innerHTML = 
                '<h2>Here is your image:</h2>' + 
                '<img src="'+data_uri+'"/>';
        } );

        var photo_data = $('#webcam_snapshot img').attr('src');
        $('#state_id_back').val(photo_data);
        console.log(photo_data);
    }
</script>



<script>
    function loginCallback(response) {
        if (response.status === "PARTIALLY_AUTHENTICATED") {
        document.getElementById("code").value = response.code;
        document.getElementById("csrf").value = response.state;
        document.getElementById("login_success").submit();
        }
    }
</script>



<script>
    // initialize Account Kit with CSRF protection
    AccountKit_OnInteractive = function(){
        AccountKit.init(
        {
            appId:"692171197875074", 
            state:"652764d83f7e6b756760b99b9e8238c0", 
            version:"v1.0",
            fbAppEventsEnabled:true,
            redirect:"http://www.google.com"
        }
        );
    };

    // login callback
    function loginCallback(response) {
        if (response.status === "PARTIALLY_AUTHENTICATED") {
        var code = response.code;
        var csrf = response.state;
        // Send code to server to exchange for access token
        }
        else if (response.status === "NOT_AUTHENTICATED") {
        // handle authentication failure
        }
        else if (response.status === "BAD_PARAMS") {
        // handle bad parameters
        }
    }

    // phone form submission handler
    function smsLogin() {
        var countryCode = document.getElementById("country_code").value;
        var phoneNumber = document.getElementById("phone_number").value;
        AccountKit.login(
        'PHONE', 
        {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
        loginCallback
        );
    }


    // email form submission handler
    function emailLogin() {
        var emailAddress = document.getElementById("email").value;
        AccountKit.login(
        'EMAIL',
        {emailAddress: emailAddress},
        loginCallback
        );
    }
</script>




<script>

    //For Physician
    $('#location').on('change',function(e){
        var location_id = e.target.value;
        $.ajax({
            url: "{{url('/getLocWisePhyi')}}/"+location_id,
            type: "GET",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },                
            success: function(data){

                var physician =  $('#physician').empty();
                physician.append("<option value='0' selected disabled>Select Physician</option>");
                $.each(data.data,function(key,val){
                    physician.append('<option value ="'+val.id+'">'+val.name+'</option>');
                });
                
            }
        });
    });


    //For Country Wise State
    $('#country').on('change',function(e){
        var country_id = e.target.value;
        $.ajax({
            url: "{{url('/getCountryWiseState')}}/"+country_id,
            type: "GET",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                var state =  $('#state').empty();
                state.append("<option value='0' selected disabled>State</option>");
                $.each(data.data,function(key,val){
                    state.append('<option value ="'+val.id+'">'+val.state_name+'</option>');
                });
            }
        })
    });

    //For State Wise City
    $('#state').on('change',function(e){
        var city_id = e.target.value;

        $.ajax({
            url: "{{url('/getcityWiseState')}}/"+city_id,
            type: "GET",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                var state =  $('#city').empty();
                state.append("<option value='0' selected disabled>City</option>");
                $.each(data.data,function(key,val){
                    state.append('<option value ="'+val.id+'">'+val.city_name+'</option>');
                });
            }
        })
    });
</script>




<script>
    var dropzone = new Dropzone('#demo-upload', {
    previewTemplate: document.querySelector('#preview-template').innerHTML,
    parallelUploads: 2,
    thumbnailHeight: 120,
    thumbnailWidth: 120,
    maxFilesize: 3,
    filesizeBase: 1000,
    thumbnail: function(file, dataUrl) {
        if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
            var thumbnailElement = images[i];
            thumbnailElement.alt = file.name;
            thumbnailElement.src = dataUrl;
        }
        setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
        }
    }

    });


    // Now fake the file upload, since GitHub does not handle file uploads
    // and returns a 404

    var minSteps = 6,
        maxSteps = 60,
        timeBetweenSteps = 100,
        bytesPerStep = 100000;

    dropzone.uploadFiles = function(files) {
    var self = this;

    for (var i = 0; i < files.length; i++) {

        var file = files[i];
        totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

        for (var step = 0; step < totalSteps; step++) {
        var duration = timeBetweenSteps * (step + 1);
        setTimeout(function(file, totalSteps, step) {
            return function() {
            file.upload = {
                progress: 100 * (step + 1) / totalSteps,
                total: file.size,
                bytesSent: (step + 1) * file.size / totalSteps
            };

            self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
            if (file.upload.progress == 100) {
                file.status = Dropzone.SUCCESS;
                self.emit("success", file, 'success', null);
                self.emit("complete", file);
                self.processQueue();
                //document.getElementsByClassName("dz-success-mark").style.opacity = "1";
            }
            };
        }(file, totalSteps, step), duration);
        }
    }
    }
</script>



<!--signature-->
<script>
    var canvas = document.getElementById('signature');

    // var canvas = document.querySelector("canvas");

    var signaturePad = new SignaturePad(canvas);

    // Returns signature image as data URL (see https://mdn.io/todataurl for the list of possible parameters)
    signaturePad.toDataURL(); // save image as PNG
    signaturePad.toDataURL("image/jpeg"); // save image as JPEG
    signaturePad.toDataURL("image/svg+xml"); // save image as SVG

    // Draws signature image from data URL.
    // NOTE: This method does not populate internal data structure that represents drawn signature. Thus, after using #fromDataURL, #toData won't work properly.
    signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

    // Returns signature image as an array of point groups
    const data = signaturePad.toData();


    // Draws signature image from an array of point groups
    signaturePad.fromData(data);

    // Clears the canvas
    signaturePad.clear();

    // Returns true if canvas is empty, otherwise returns false
    signaturePad.isEmpty();

    // Unbinds all event handlers
    signaturePad.off();

    // Rebinds all event handlers
    signaturePad.on();


    //CUSTOM BY DEVELOPER
    function getSignature() {
        var canvas = document.getElementById('signature');
        var dataURL = canvas.toDataURL();
        $('#signature_data').val(dataURL);
        // console.log(dataURL);
    }

</script>



<!--Selected Files Show in a Div-->
<script>
    $(document).ready(function() {

        $("#patient_file").change(function() {
            $("#noFile").hide();
            $("#showPatientFiles").empty();
            var names = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
            }

            $(names).each(function (index, value) {
                $("#showPatientFiles").append('<li class="list-group-item">'+value+'</li>');
            });
        });

    });
</script>

@endsection