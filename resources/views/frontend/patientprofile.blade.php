@extends('frontend.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ic-apply-field ic-sign-in">


                    @if(session()->has('success'))
                        <p class="alert alert-success" style="text-align: center;">
                            {{ session()->get('success') }}
                        </p>
                    @endif


                    <div class="row">
                        <div class="col-lg-12 col-md-12 m-auto">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li>
                                    <a href="{{ action('PatientController@downloadPatientInfo') }}" class="btn btn-success">DOWNLOAD INFORMATION</a>
                                </li>
                                <li class="nav-item">
                                    <!-- logout button -->
                                    <div id="logout" class="btn-header transparent pull-right">
                                        <span> 
                                            <a class="btn btn-info" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out"></i>LOGOUT
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form> 
                                        </span>
                                    </div>                    
                                    <!-- end logout button -->
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                @if(count($patients))
                                    @foreach($patients as $patient)
                                        <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab" style="margin-bottom:50px">
                                            <div class="col-lg-12">
                                                <form>
                                                    <h2><i class="fas fa-plus-circle"></i> General Information</h2>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Location/Clinic</label>
                                                            <input type="text" disabled name="location_id" value="{{ $patient->clinic_name }}" class="form-control"> 
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Physician</label>
                                                            <input type="text" disabled name="physician_id" value="{{ $patient->physician_name }}" class="form-control">
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>First Name</label>
                                                            <input type="text" disabled name="f_name" value="{{ $patient->f_name }}" class="form-control"> 
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Last Name</label>
                                                            <input type="text" disabled name="l_name" value="{{ $patient->l_name }}" class="form-control">
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Email</label>
                                                            <input type="email" disabled name="email" value="{{ $patient->email }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-sm-6 col-md-6">
                                                            <label>Phone Number</label>
                                                            <input type="text" disabled name="phone_number" value="{{ $patient->phone_number }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                                            <label>Address</label>
                                                            <input type="text" disabled name="address" value="{{ $patient->address }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Country</label>
                                                            <input type="text" disabled name="country" value="{{ $patient->country }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-sm-6 col-md-6">
                                                                    <label>State</label>
                                                                    <input type="text" disabled name="state" value="{{ $patient->state }}" class="form-control">
                                                                </div>
                                                                <div class="col-lg-6 col-sm-6 col-md-6">
                                                                    <label>City</label>
                                                                    <input type="text" disabled name="city" value="{{ $patient->city }}" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>State ID</label>
                                                            <input type="text" disabled name="state_id" value="{{ $patient->state_id }}" class="form-control">
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>ZIP Code</label>
                                                            <input type="text" disabled name="zip_code" value="{{ $patient->zip_code }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Date of Birth</label>
                                                            <input type="text" disabled name="dob" value="{{ $patient->dob }}" class="form-control datepicker-here" data-language='en'> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Gender</label>
                                                            <input type="text" disabled name="gender" value="{{ $patient->gender }}" class="form-control">
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Initial Visit</label>
                                                            <input type="text" disabled name="initial_visit" value="{{ $patient->initial_visit }}" class="form-control datepicker-here" data-language='en'> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Expiration</label>
                                                            <input type="text" disabled name="expiration" value="{{ $patient->expiration }}" class="form-control datepicker-here" data-language='en'>
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Caregiver</label>
                                                            <input type="text" disabled name="caregiver" value="{{ $patient->caregiver }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Internal #</label>
                                                            <input type="text" disabled name="internal" value="{{ $patient->internal }}" class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Social Security Number</label>
                                                            <input type="password" disabled name="social_security_no" value="{{ $patient->social_security_no }}"  class="form-control"> 
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Rec Type</label>
                                                            <input type="text" disabled name="rec_type" value="{{ $patient->rec_type }}" class="form-control">
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Parent / Guardian</label>
                                                            <input type="text" disabled name="guardian" value="{{ $patient->guardian }}" class="form-control">
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 ic-profile-radio mb-3 mb-lg-0">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <label>Contact Options</label>
                                                                </div>
                                                                <div class="col-lg-6 col-6">
                                                                    <label class="ic-checkbox">
                                                                        @if($patient->contact_option === "Email")
                                                                            <input type="radio" checked disabled name="contact_option" value="Email">
                                                                        @else
                                                                            <input type="radio" disabled name="contact_option" value="Email">
                                                                        @endif
                                                                        <span class="checktext">Email</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6 col-6">
                                                                    <label class="ic-checkbox">                                                                    
                                                                        @if($patient->contact_option === "SMS")
                                                                            <input type="radio" checked disabled name="contact_option" value="SMS">
                                                                        @else
                                                                            <input type="radio" disabled name="contact_option" value="SMS">
                                                                        @endif
                                                                        <span class="checktext">SMS</span>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>Referred By</label>
                                                            <input type="text" disabled name="referred_by" value="{{ $patient->referred_by }}" class="form-control">
                                                        </div>

                                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                                            <label>MMU ID #</label>
                                                            <input type="text" disabled name="mmu_id" value="{{ $patient->mmu_id }}" class="form-control"> 
                                                        </div>
                                                        <!-- <div class="col-lg-12 text-right">
                                                            <button type="button" class="mr-3">Previous</button>
                                                            <button type="button">Next</button>
                                                        </div> -->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="tab-pane fad show active" id="step3" role="tabpanel" aria-labelledby="step3-tab">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form>
                                                        <h2>
                                                            <i class="fas fa-plus-circle"></i> 
                                                            Drivers License / State ID And Photo
                                                        </h2>

                                                        <div class="row">
                                                            <div class="col-lg-12 text-center">
                                                                <div class="row">
                                                                    <div class="col-sm-6 col-lg-4">
                                                                        <div class="ic-profile-upload">
                                                                            <img src="{{ asset('storage/images/'.$patient->state_id_front) }}" width="100%" height="100%">
                                                                        </div>
                                                                        <label>Drivers Lic / State ID Front</label>
                                                                    </div>
                                                                    <div class="col-sm-6 col-lg-4">
                                                                        <div class="ic-profile-upload">                                                                
                                                                            <img src="{{ asset('storage/images/'.$patient->state_id_back) }}" width="100%" height="100%">
                                                                        </div>
                                                                        <label>Drivers Lic / State ID Back</label>
                                                                    </div>
                                                                    <div class="col-sm-6 col-lg-4">
                                                                        <div class="ic-profile-upload">
                                                                            <img src="{{ asset('storage/images/'.$patient->patient_photo_id) }}" width="100%" height="100%">
                                                                        </div>
                                                                        <label>Patient Photo ID Card</label>
                                                                    </div>
                                                                    <div class="offset-lg-4"></div>
                                                                </div>
                                                            </div>


                                                            <div class="col-lg-12 mt-4 mt-lg-5">

                                                                <div class="ic-patient-files">
                                                                    <div class="row">
                                                                        <div class="col-lg-6 col-md-6">
                                                                            <h2>
                                                                                <i class="fas fa-plus-circle"></i> 
                                                                                Physician / Stuff Signature
                                                                            </h2>
                                                                            <table class="table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th scope="col" width="50%">Signature Name</th>
                                                                                        <th scope="col">Signature</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            {{ $patient->signature_name }}
                                                                                        </td>
                                                                                        <td>
                                                                                            <img src="{{ asset('storage/signature/'.$patient->signature_data) }}" width="50%" height="50%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>

                                                                        </div>

                                                                        
                                                                        <div class="col-lg-6 col-md-6">
                                                                            <h2>
                                                                                <i class="fas fa-plus-circle"></i> 
                                                                                Notes
                                                                            </h2>
                                                                            <label>Additional patient notes</label>
                                                                            <textarea disabled class="form-control">{{ $patient->patient_note }}</textarea>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="col-lg-12 mt-4 mt-lg-5">
                                                    <div class="ic-patient-files" style="margin-bottom:50px">
                                                        <div class="row">

                                                            <div class="col-lg-12 col-md-12">
                                                                <h2>
                                                                    <i class="fas fa-plus-circle"></i> 
                                                                    Patient Files
                                                                </h2>
                                                                <div class="ic-patient-form" style="height: fit-content;">
                                                                    <ul class="ic-patient-head">
                                                                        <li>
                                                                            File Name
                                                                        </li>
                                                                    </ul>
                                                                    <div class="ic-patient-body">
                                                                        <div class="list-group">
                                                                            @if(count($files))
                                                                                @foreach($files as $key=> $file)                        
                                                                                <a href="{{ asset('storage/files/'.$file->file_name) }}" target="_blank" class="list-group-item list-group-item-action">
                                                                                    {{ $file->file_org_name }}
                                                                                </a>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    @endforeach
                                @endif


                                <div class="tab-pane fade show active" id="step2" role="tabpanel" aria-labelledby="step2-tab">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <form>
                                                @if(count($sections))
                                                    @foreach($sections as $key=>$section)
                                                        <div class="col-lg-12">

                                                            <h2><i class="fas fa-plus-circle"></i>{{ $section->title }}</h2>
                                                            <h5>{{ $section->sub_title }}</h5>
                                                            <p>{{ $section->description }}</p>
                                                    
                                                            <!--Loop for checkbox-->
                                                            @foreach($section->questions as $question)

                                                                @if($question->question_type == 1)

                                                                <p>{{ $question->question }}</p>
                                                                <div class="row">
                                                                    @foreach($question->options as $option)                                                                        
                                                                        @if(count($answers))
                                                                            @foreach($answers as $answer)
                                                                                @if($answer->question_type == 1 && $answer->question_id == 1)
                                                                                                                                                                                
                                                                                    <?php $answerdata = json_decode($answer->answer, true); ?>
                                                                                    
                                                                                    @foreach($answerdata as $value)

                                                                                        @if($option->id == $value)
                                                                                        <div class="col-6 col-sm-6 col-md-4 col-lg-3 ic-profile-checkbox">
                                                                                            <label class="ic-checkbox">
                                                                                                <input type="checkbox" checked disabled value="{{ $option->id }}" name="medical_symtomps[{{ $option->id }}]">
                                                                                                <span class="checktext">{{ $option->option }}</span>
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                            </div>
                                                                                        @endif
                                                                                        
                                                                                    @endforeach
                                                                        
                                                                                @endif
                                                                            @endforeach
                                                                        @endif                                                                        
                                                                    @endforeach
                                                                </div>
                                                                @endif
                                                            @endforeach



                                                            <!--Loop for input box-->
                                                            @foreach($section->questions as $question)
                                                                @if($question->question_type == 2)
                                                                    <div class="row">                                                            
                                                                        @foreach($question->options as $option)
                                                                            <div class="col-6 col-lg-6 col-md-6">
                                                                                <label>{{ $question->question }}</label>

                                                                                @if(count($answers))
                                                                                    @foreach($answers as $answer)
                                                                                        @if($answer->question_type == 2 && ($question->id === $answer->question_id))
                                                                                            <input type="text" name="significant_issue" value="{{ $answer->answer }}" disabled class="form-control">
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif

                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                            

                                                            <!--Loop for radio button-->
                                                            @foreach($section->questions as $question)
                                                                @if($question->question_type == 3)
                                                                    <div class="row">
                                                                        @foreach($question->options as $option)
                                                                            <div class="col-lg-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-md-4 col-lg-4 ic-profile-round-radio">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-12">
                                                                                                <label>{{ $question->question }}</label>
                                                                                            </div>

                                                                                            @if(count($answers))
                                                                                                @foreach($answers as $answer)
                                                                                                    @if($answer->question_type == 3 && ($question->id === $answer->question_id))
                                                                                                    <div class="col-lg-12">
                                                                                                        <ul class="ic-yes-no">
                                                                                                            <li>
                                                                                                                <label class="ic-checkbox">
                                                                                                                @if($answer->answer == 1)
                                                                                                                    <input type="radio" value="1" checked="checked" name="medical_info[{{$question->id}}]">
                                                                                                                @else
                                                                                                                    <input type="radio" value="1" disabled name="medical_info[{{$question->id}}]">
                                                                                                                @endif
                                                                                                                    <span class="checktext">YES</span>
                                                                                                                    <span class="checkmark"></span>
                                                                                                                </label>
                                                                                                            </li>
                                                                                                            <li>
                                                                                                                <label class="ic-checkbox">
                                                                                                                    @if($answer->answer == 0)
                                                                                                                        <input type="radio" value="0" checked="checked" name="medical_info[{{$question->id}}]">
                                                                                                                    @else
                                                                                                                        <input type="radio" value="0" disabled name="medical_info[{{$question->id}}]">
                                                                                                                    @endif
                                                                                                                        <span class="checktext">No</span>
                                                                                                                        <span class="checkmark"></span>
                                                                                                                </label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            @endforeach

                                                        </div>
                                                    @endforeach
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                </div>

                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection