<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MedCardNow</title>

    <!-- ===== Stylesheet Files Link ===== -->

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">

</head>

<body>

    <!-- ===== Header Part HTML Start ===== -->

    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('frontend/images/logo.png') }}" alt="Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fas fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mr-lg-4">
                        <a class="nav-link" href="index.html"><i class="fas fa-chevron-left"></i> Back To Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('frontend/images/nav-logo1.png') }}" alt="Hippo">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('frontend/images/nav-logo2.png') }}" alt="Secure">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- ===== Header Part HTML End ===== -->

    <!-- ===== Application Form Part HTML Start ===== -->

    <section id="ic-apply-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ic-apply-txt text-center">
                        <h5>Online Medical Evaluation - <span class="ic-green">Money Back Guarantee</span> - <span
                                class="ic-sky">Instant Rec Download</span></h5>
                        <h6>Welcome to Med Card Now California’s Online Medical Marijuana Certification System</h6>
                        <div class="ic-apply-notice" style="display:none;">
                            <h6>Currently there are 0 physicians online.</h6>
                            <h6>Don't Worry, You Can Schedule Your Online Consultation Below.</h6>
                        </div>
                    </div>
                    <div class="ic-apply-field">
                        <div class="row">
                            <div class="col-lg-10 m-auto">
                                <h6>Application Form</h6>
                                <form id="BookAndReg">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <label>Patient Type</label>
                                            <select id="ctrlBook">
                                                <option value="">Select</option>
                                                <option value="0">New Patient</option>
                                                <option value="1">Old Patient</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Select Physician</label>
                                            <select>
                                                <option value="0">Select Physician</option>
                                                <option value="1">Physician One</option>
                                                <option value="2">Physician Two</option>
                                                <option value="3">Physician Three</option>
                                                <option value="4">Physician Four</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>First Name</label>
                                            <input type="text" placeholder="Enter First Name" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Last Name</label>
                                            <input type="text" placeholder="Enter Last Name" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Email</label>
                                            <input type="email" placeholder="Enter Email" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Phone</label>
                                            <input type="text" placeholder="Enter Phone" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Password</label>
                                            <input type="password" placeholder="Enter Password" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Confirm Password</label>
                                            <input type="password" placeholder="Confirm Password" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Date</label>
                                            <input type="text" placeholder="DD/MM/YYYY"
                                                class="form-control datepicker-here" data-language='en'>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Time</label>
                                            <input type="text" placeholder="Hours : Minutes" class="form-control">
                                        </div>
                                        <div class="col-lg-12 text-center">
                                            <button type="submit">Book Appointment</button>
                                        </div>
                                    </div>
                                </form>

                                <form id="onlyBooks" style="display:none;">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <label>Patient Type</label>
                                            <select id="ctrlBookAndReg">
                                                <option value="">Select</option>
                                                <option value="0">New Patient</option>
                                                <option value="1">Old Patient</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Select Physician</label>
                                            <select>
                                                <option value="0">Select Physician</option>
                                                <option value="1">Physician One</option>
                                                <option value="2">Physician Two</option>
                                                <option value="3">Physician Three</option>
                                                <option value="4">Physician Four</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Email</label>
                                            <input type="email" placeholder="Enter Email" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Password</label>
                                            <input type="password" placeholder="Enter Password" class="form-control">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Date</label>
                                            <input type="text" placeholder="DD/MM/YYYY"
                                                class="form-control datepicker-here" data-language='en'>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <label>Time</label>
                                            <input type="text" placeholder="Hours : Minutes" class="form-control">
                                        </div>
                                        <div class="col-lg-12 text-center">
                                            <button type="submit">Book Appointment</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ===== Application Form Part HTML End ===== -->

    <!-- ===== Footer Part HTML Start ===== -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-content">
                        <div class="row">
                            <div class="col-lg-6 m-auto text-center">
                                <div class="footer-inner-content">
                                    <img src="{{ asset('frontend/images/footer-logo.png') }}" alt="Footer Logo" class="footer-logo">
                                    <p>Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. </p>
                                    <img src="{{ asset('frontend/images/payment.png') }}" alt="Payment Card's Image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 m-auto text-center">
                            <div class="cpryt">
                                <p>Copyright © 2019 Medical Cannabis Card Now - All Rights Reserved.
                                    Design & Developed With <i class="fas fa-heart"></i> By <a href="https://itclanbd.com/"
                                        target="blank">
                                        <img src="{{ asset('frontend/images/ITclanBD.svg') }}" alt="ITclanBD">
                                    </a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- ===== Footer Part HTML End ===== -->




    <!-- ===== Javascript Files Source Path ===== -->


    <script src="{{ asset('frontend/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/slick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.en.js') }}"></script>
    <script src="{{ asset('frontend/js/custom.js') }}"></script>



    

    <script type="text/Javascript">
        $('#ctrlBook').on('change', function () {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if(valueSelected == 1){
                // $("#onlyBooks").show();
                $("#onlyBooks").css("display", "block");
            }
            else{
                // $("#onlyBooks").hide();
                $("#onlyBooks").css("display", "none");
            }
        });


        $('#ctrlBookAndReg').on('change', function () {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if(valueSelected == 1){
                $("#BookAndReg").show();
            }
            else{
                $("#BookAndReg").hide();
            }
        });

    </script>



</body>

</html>