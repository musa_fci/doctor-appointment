    <!-- ===== Breadcumb Part HTML Start ===== -->

    <section id="ic-breadcumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Patient Profile</h3>
                    <h6><a href="index.html">Home</a> // <a href="profile.html">Patient Profile</a></h6>
                </div>
            </div>
        </div>
    </section>

    <!-- ===== Breadcumb Part HTML End ===== -->