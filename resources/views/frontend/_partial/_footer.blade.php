    <!-- ===== Footer Part HTML Start ===== -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-content">
                        <div class="row">
                            <div class="col-lg-6 m-auto text-center">
                                <div class="footer-inner-content">
                                    <img src="{{ asset('frontend/images/footer-logo.png') }}" alt="Footer Logo" class="footer-logo">
                                    <p>Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. </p>
                                    <img src="{{ asset('frontend/images/payment.png') }}" alt="Payment Card's Image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 m-auto text-center">
                            <div class="cpryt">
                                <p>Copyright © 2019 Medical Cannabis Card Now - All Rights Reserved.
                                    Design & Developed With <i class="fas fa-heart"></i> By <a
                                        href="https://itclanbd.com/" target="blank">
                                        <img src="{{ asset('frontend/images/ITclanBD.svg') }}" alt="ITclanBD">
                                    </a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- ===== Footer Part HTML End ===== -->