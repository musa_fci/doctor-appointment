
    <!-- ===== Javascript Files Source Path ===== -->

    <script src="{{ asset('frontend/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/slick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontend/js/datepicker.en.js') }}"></script>
    <script src="{{ asset('frontend/js/webcam.min.js') }}"></script>
    <script src="{{ asset('frontend/js/dropzone.js') }}"></script>
    <script src="{{ asset('frontend/js/custom.js') }}"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>


    @section('script')
    @show
