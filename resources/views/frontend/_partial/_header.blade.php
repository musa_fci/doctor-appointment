    <!-- ===== Header Part HTML Start ===== -->

    <section id="header">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-5 px-sm-0 px-lg-3">
                        <a href="index.html" class="header-brand">
                            <img src="{{ asset('frontend/images/logo.png') }}" alt="Logo">
                        </a>
                    </div>
                    <div class="col-lg-6 col-sm-7 px-sm-0 px-lg-3">
                        <div class="header-info text-lg-right text-sm-right">
                            <h6>New & Renewals only $39.99 Embossed Copy Included!<br> Only pay if you're approved!</h6>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <div class="profile mr-auto d-block d-lg-none">
                    <a href="#"><img src="{{ asset('frontend/images/profile-pic.png') }}" alt="Profile Picture"> Jesse Williamson</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="fas fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.html">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Conditions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Apply Now</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Patient varification</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Locations</a>
                        </li>
                    </ul>
                    <div class="profile ml-auto d-none d-lg-block">
                        <a href="profile.html"><img src="{{ asset('frontend/images/profile-pic.png') }}" alt="Profile Picture"> Jesse
                            Williamson</a>
                    </div>
                </div>
            </div>
        </nav>
    </section>

    <!-- ===== Header Part HTML End ===== -->