
require('./bootstrap');


window.Vue = require('vue');


import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

//Dashboard Components
import Dashboard from './components/backend/Dashboard.vue';
Vue.component('Dashboard', Dashboard);


//Admin Components
import AdminProfile from './components/backend/AdminProfile.vue';
Vue.component('AdminProfile','AdminProfile');


//Clinic/Hospital/Location Components
import LocationAdd from './components/backend/LocationAdd.vue';
Vue.component('LocationAdd','LocationAdd');

import LocationManage from './components/backend/LocationManage.vue';
Vue.component('LocationManage','LocationManage');


//Physician Components
import PhysicianAdd from './components/backend/PhysicianAdd.vue';
Vue.component('PhysicianAdd','PhysicianAdd');

import PhysicianManage from './components/backend/PhysicianManage.vue';
Vue.component('PhysicianManage','PhysicianManage');



//Staff Components
import StaffAdd from './components/backend/StaffAdd.vue';
Vue.component('StaffAdd','StaffAdd');

import StaffManage from './components/backend/StaffManage.vue';
Vue.component('StaffManage','StaffManage');



//Module Components
import ModuleAdd from './components/backend/ModuleAdd.vue';
Vue.component('ModuleAdd','ModuleAdd');

import ModuleManage from './components/backend/ModuleManage.vue';
Vue.component('ModuleManage','ModuleManage');

import ModuleEdit from './components/backend/ModuleEdit.vue';
Vue.component('ModuleEdit','ModuleEdit');



//Module Wise Role Components
import ModuleWiseRoleAdd from './components/backend/ModuleWiseRoleAdd.vue';
Vue.component('ModuleWiseRoleAdd','ModuleWiseRoleAdd');

import ModuleWiseRoleManage from './components/backend/ModuleWiseRoleManage.vue';
Vue.component('ModuleWiseRoleManage','ModuleWiseRoleManage');


//Staff Category Components
import StaffCatAdd from './components/backend/StaffCatAdd.vue';
Vue.component('StaffCatAdd','StaffCatAdd');

import StaffCatManage from './components/backend/StaffCatManage.vue';
Vue.component('StaffCatManage','StaffCatManage');


//Patient Setting
import PatientSectionAdd from './components/backend/PatientSectionAdd.vue';
Vue.component('PatientSectionAdd','PatientSectionAdd');

import PatientSectionManage from './components/backend/PatientSectionManage.vue';
Vue.component('PatientSectionManage','PatientSectionManage');

import PatientQuestionAdd from './components/backend/PatientQuestionAdd.vue';
Vue.component('PatientQuestionAdd','PatientQuestionAdd');

import PatientQuestionManage from './components/backend/PatientQuestionManage.vue';
Vue.component('PatientQuestionManage','PatientQuestionManage');

import PatientQuestionView from './components/backend/PatientQuestionView.vue';
Vue.component('PatientQuestionView','PatientQuestionView');


import PatientOptionAdd from './components/backend/PatientOptionAdd.vue';
Vue.component('PatientOptionAdd','PatientOptionAdd');


//Patient
import PatientAdd from './components/backend/PatientAdd.vue';
Vue.component('PatientAdd','PatientAdd');

import AllPatient from './components/backend/AllPatient.vue';
Vue.component('AllPatient','AllPatient');

const routes = [
    { path: '/', name: '/', component: Dashboard },
    { path: '/AdminProfile', name: 'AdminProfile', component: AdminProfile },
    { path: '/LocationAdd', name: 'LocationAdd', component: LocationAdd },
    { path: '/LocationManage', name: 'LocationManage', component: LocationManage },
    { path: '/PhysicianAdd', name: 'PhysicianAdd', component: PhysicianAdd },
    { path: '/PhysicianManage', name: 'PhysicianManage', component: PhysicianManage },
    { path: '/StaffAdd', name: 'StaffAdd', component: StaffAdd },
    { path: '/StaffManage', name: 'StaffManage', component: StaffManage },
    { path: '/ModuleAdd', name: 'ModuleAdd', component:ModuleAdd },
    { path: '/ModuleManage', name: 'ModuleManage', component:ModuleManage },
    { path: '/ModuleWiseRoleAdd', name: 'ModuleWiseRoleAdd', component:ModuleWiseRoleAdd },
    { path: '/ModuleWiseRoleManage', name: 'ModuleWiseRoleManage', component:ModuleWiseRoleManage },
    { path: '/StaffCatAdd', name: 'StaffCatAdd', component:StaffCatAdd },
    { path: '/StaffCatManage', name: 'StaffCatManage', component:StaffCatManage },
    { path: '/PatientSectionAdd', name: 'PatientSectionAdd', component:PatientSectionAdd },
    { path: '/PatientSectionManage', name: 'PatientSectionManage', component:PatientSectionManage },
    { path: '/PatientQuestionAdd', name: 'PatientQuestionAdd', component:PatientQuestionAdd },
    { path: '/PatientQuestionManage', name: 'PatientQuestionManage', component:PatientQuestionManage },
    { path: '/PatientQuestionView', name: 'PatientQuestionView', component:PatientQuestionView },
    { path: '/PatientAdd', name: 'PatientAdd', component:PatientAdd },
    { path: '/AllPatient', name: 'AllPatient', component:AllPatient },



    { path: '/PatientOptionAdd', name: 'PatientOptionAdd', component:PatientOptionAdd },
  ]


const router = new VueRouter({
    routes,
    mode: 'history',
    // history: true,
    // base: '/Dashboard'
})


const app = new Vue({
    el: '#app',
    router
    
});